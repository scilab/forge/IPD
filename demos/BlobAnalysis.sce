////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
function BlobAnalysis_demo()

  DemoPath = get_absolute_file_path('BlobAnalysis.sce');
  RGB = ReadImage(DemoPath + 'teaset.png');
  Image = RGB2Gray(RGB);

  clear RGB;
  scf();

  OriginalFigure = ShowImage(Image, 'Intensity Image');
  Image = uint8(255 * ones(size(Image, 1), size(Image, 2))) - Image;
  Threshold = 100;
  ThresholdImage = SegmentByThreshold(Image, Threshold);
  clear Image;
  ObjectImage = SearchBlobs(ThresholdImage);
  clear ThresholdImage;
  ColorMapSize = max(3, max(ObjectImage));

  scf();
  ShowImage(ObjectImage, 'Object Image', jetcolormap(ColorMapSize));
  ObjectImage = FilterBySize(ObjectImage, 10000);
  ColorMapSize = max(3, max(ObjectImage));

  scf();
  ShowImage(ObjectImage, ...
          'Filtered Object Image', ...
          jetcolormap(ColorMapSize));

  IsCalculated = CreateFeatureStruct();

  IsCalculated.BoundingBox = %t;
  BlobStatistics = AnalyzeBlobs(ObjectImage, IsCalculated);
  DrawBoundingBoxes(BlobStatistics, [1 1 1]);
  DrawBoundingBoxes(BlobStatistics, [1 0 0], OriginalFigure);
  demo_viewCode('BlobAnalysis.sce');
endfunction
////////////////////////////////////////////////////////////////////////////
BlobAnalysis_demo();
clear BlobAnalysis_demo;
////////////////////////////////////////////////////////////////////////////
