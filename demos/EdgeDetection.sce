////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
function EdgeDetection_demo()
  global EDGE_SOBEL;
  DemoPath = get_absolute_file_path('EdgeDetection.sce');
  RGB = ReadImage(DemoPath + 'teaset.png');
  Image = RGB2Gray(RGB);
  clear RGB;

  scf();
  ShowImage(Image, 'Intensity Image');
  Gradient = EdgeFilter(Image, EDGE_SOBEL);
  clear Image;

  scf();
  ShowImage(Gradient, 'Gradient Image');
  EdgeImage = SegmentByThreshold(Gradient, 60);
  clear Gradient;

  scf();
  ShowImage(EdgeImage, 'Edge Image');
  demo_viewCode('EdgeDetection.sce');
endfunction
////////////////////////////////////////////////////////////////////////////
EdgeDetection_demo();
clear EdgeDetection_demo;
////////////////////////////////////////////////////////////////////////////
