////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
function Morphology_demo()

  disp('Image = zeros(10, 12); Image(5, 2) = 1; Image(2 : 9, 5 : 8) = 1;');
  Image = zeros(10, 12);
  Image(5, 2) = 1;
  Image(2 : 9, 5 : 8) = 1;
  disp(Image);
  disp('StructuringElement = CreateStructureElement(''square'', 3);');
  StructuringElement = CreateStructureElement('square', 3);
  disp(StructuringElement);
  disp('FilteredImage = OpenImage(Image, StructuringElement);');
  FilteredImage = OpenImage(Image, StructuringElement);
  disp(FilteredImage);

endfunction
////////////////////////////////////////////////////////////////////////////
Morphology_demo()
clear Morphology_demo;
////////////////////////////////////////////////////////////////////////////
