///////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////
function TemplateMatching_demo()

  global MATCH_CORRELATION_NORM;
  DemoPath = get_absolute_file_path('TemplateMatching.sce');

  SourceColorImage = ReadImage(DemoPath + 'teaset.png');

  SourceImage = RGB2Gray(SourceColorImage);

  TemplateColorImage = ReadImage(DemoPath + 'cropped_image.png');

  TemplateImage = RGB2Gray(TemplateColorImage);

  Match = MatchTemplate(SourceImage, ...
                      TemplateImage, ...
                      MATCH_CORRELATION_NORM);
                      
  MatchList = FindBestMatches(Match, ...
                            size(TemplateImage), ...
                            MATCH_CORRELATION_NORM);
                            
  SquareList = list();
                            
  for n = 1 : size(MatchList, 1)
    SquareList($ + 1) = struct('BoundingBox', ...
                            cat(2, MatchList(n, :) - [1 1], [3 3])');
  end;

  SourceHandle = scf();
  ShowColorImage(SourceColorImage, 'Image with Marked Match Positions');
  DrawBoundingBoxes(SquareList, [0 1 0], SourceHandle);
  TemplateHandle = figure();
  ShowColorImage(TemplateColorImage, 'Template Image');
  demo_viewCode('TemplateMatching.sce');
  
endfunction
///////////////////////////////////////////////////////////////////////
TemplateMatching_demo();
clear TemplateMatching_demo;
///////////////////////////////////////////////////////////////////////
