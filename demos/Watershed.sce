////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
function Watershed_demo()
  global EDGE_SOBEL;
  DemoPath = get_absolute_file_path('Watershed.sce');
  RGB = ReadImage(DemoPath + 'teaset.png');
  Image = RGB2Gray(RGB);
  clear RGB;
  Gradient = EdgeFilter(Image, EDGE_SOBEL);
  clear Image;

  scf();
  ShowImage(Gradient, 'Gradient Image', jetcolormap(256));
  EdgeImage = ~SegmentByThreshold(Gradient, 60);
  DistanceImage = DistanceTransform(EdgeImage);
  clear EdgeImage;

  ThresholdImage = SegmentByThreshold(DistanceImage, 8);
  clear DistanceImage;
  MarkerImage = SearchBlobs(ThresholdImage);
  scf();
  ShowImage(MarkerImage, 'Marker Image', jetcolormap(64));
  SegmentedImage = Watershed(Gradient, MarkerImage);
  clear Gradient;
  clear MarkerImage;

  scf();
  ColorMapLength = length(unique(SegmentedImage));
  ShowImage(SegmentedImage, ...
          'Result of Watershed Transform', ...
          jetcolormap(ColorMapLength));

  demo_viewCode('Watershed.sce');
endfunction
////////////////////////////////////////////////////////////////////////////
Watershed_demo();
clear Watershed_demo;
////////////////////////////////////////////////////////////////////////////
