//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
function waveletframe_demo()
  global IPD_PATH;
  
  global WAVELET_DAUBECHIES_2;
  DemoPath = IPD_PATH + 'demos\';
  RGB = ReadImage(DemoPath + 'small_teaset.png');
  Image = RGB2Gray(RGB);
  WaveletFrames = CreateWaveletFrames(Image, WAVELET_DAUBECHIES_2, 3);
  ChannelVariance = ComputeChannelVariance(WaveletFrames, [9 9]);

  ListOfTitles = ['Horizontal Detail on 1st Level'; ...
                  'Diagonal Detail on 1st Level'; ...
                  'Vertical Detail on 1st Level'; ...
                  'Horizontal Detail on 2nd Level'; ...
                  'Diagonal Detail on 2nd Level'; ...
                  'Vertical Detail on 2nd Level'; ...
                  'Horizontal Detail on 3rd Level'; ...
                  'Diagonal Detail on 3rd Level'; ...
                  'Vertical Detail on 3rd Level'; ...
                  'Approximation on 3rd Level']; 

   scf(); 
   ShowImage(Image, 'Original Image');

   for n = 1 : size(ChannelVariance, 3)
     Image = ChannelVariance(:, :, n);
     scf();
     ShowImage(Image / max(Image), ListOfTitles(n));  
   end;
   demo_viewCode('WaveletFrames.sce');
endfunction
////////////////////////////////////////////////////////////////////////////
waveletframe_demo();
clear waveletframe_demo;
//////////////////////////////////////////////////////////////////////////// 
