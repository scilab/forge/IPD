<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:xi="http://www.w3.org/2001/XInclude"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:html="http://www.w3.org/1999/xhtml"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv xml:id="FindBestMatches">
    <refname>FindBestMatches</refname>

    <refpurpose>sucht nach den Positionen, an denen ein Musterbild am besten
    mit einem Bild übereinstimmt</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Aufruf</title>

    <synopsis>ListePositionen = FindBestMatches(Aehnlichkeit, GroesseMusterbild, Methode);</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameter</title>

    <variablelist>
      <varlistentry>
        <term>Aehnlichkeit</term>

        <listitem>
          <para>2D-Matrix vom Typ double, ist das Ergebnis von
          MatchTemplate</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>GroesseMusterbild</term>

        <listitem>
          <para>Vektor mit zwei Elementen, gibt die Anzahl der Zeilen und
          Spalten des Musterbildes an</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>Methode</term>

        <listitem>
          <para>Konstante vom Typ uint8, folgende Werte sind möglich:</para>

          <itemizedlist>
            <listitem>
              <para>MATCH_LEAST_SQUARES: Summe der quadratischen Differenzen
              zwischen den Grauwerten der Pixel von Quell- und
              Musterbild</para>
            </listitem>

            <listitem>
              <para>MATCH_CORRELATION: Korrelation zwischen Musterbild und
              Umgebung von Mittelpunkt des Musterbildes im Quellbild</para>
            </listitem>

            <listitem>
              <para>MATCH_LEAST_SQUARES_NORM: normalisierte Summe der
              quadratischen Differenzen zwischen den Grauwerten der Pixel von
              Quell- und Musterbild</para>
            </listitem>

            <listitem>
              <para>MATCH_CORRELATION_NORM: normalisierte Korrelation zwischen
              Musterbild und Umgebung von Mittelpunkt des Musterbildes im
              Quellbild</para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>ListePositionen</term>

        <listitem>
          <para>Matrix mit zwei Spalten, enthält die Positionen aller Pixel,
          bei denen die Ähnlichkeit optimal ist. Die erste Spalte gibt die x-,
          die zweite Spalte die y-Koordinate an.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Beschreibung</title>

    <para>Diese Funktion sucht nach den Positionen, an denen ein Musterbild am
    besten mit einem Bild übereinstimmt. Die Koordinaten der gefundenen Pixel
    werden dabei auf die Größe des ursprünglichen Bildes umgerechnet.</para>
  </refsection>

  <refsection>
    <title>Beispiel</title>

    <programlisting role="example">global MATCH_CORRELATION_NORM;

global IPD_PATH;

QuellbildFarbe = ReadImage(IPD_PATH + 'demos\teaset.png');

Quellbild = RGB2Gray(QuellbildFarbe);

MusterbildFarbe = ReadImage(IPD_PATH + 'demos\cropped_image.png');

Musterbild = RGB2Gray(MusterbildFarbe);

Aehnlichkeit = MatchTemplate(Quellbild, Musterbild, MATCH_CORRELATION_NORM);
                      
BesteUebereinstimmung = FindBestMatches(Aehnlichkeit, size(Musterbild), MATCH_CORRELATION_NORM);
                            
Markierung = list();
                            
for n = 1 : size(BesteUebereinstimmung, 1)
  
 Markierung($ + 1) = struct('BoundingBox', cat(2, BesteUebereinstimmung(n, :) - [1 1], [3 3])');
  
end;                            

QuellbildFenster = figure();
                            
ShowColorImage(QuellbildFarbe, 'Bild mit markierten Positionen der besten Übereinstimmung');

DrawBoundingBoxes(Markierung, [0 1 0], QuellbildFenster);

MusterbildFenster = figure();

ShowColorImage(MusterbildFarbe, 'Musterbild');</programlisting>
  </refsection>

  <refsection>
    <title>Siehe auch</title>

    <simplelist type="inline">
      <member><link linkend="MatchTemplate">MatchTemplate</link></member>
    </simplelist>
  </refsection>
</refentry>
