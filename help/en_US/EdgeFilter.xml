<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:xi="http://www.w3.org/2001/XInclude"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:html="http://www.w3.org/1999/xhtml"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv xml:id="EdgeFilter">
    <refname>EdgeFilter</refname>

    <refpurpose>applies filter for edge detection to gray level
    image</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>FilteredImage = EdgeFilter(Image, FilterType);</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>Image</term>

        <listitem>
          <para>2D matrix of type uint8, uint16, uint32 or double</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>FilterType</term>

        <listitem>
          <para>scalar of type uint8, can be EDGE_SOBEL, EDGE_LAPLACE,
          EDGE_PREWITT or EDGE_SCHARR.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>FilteredImage</term>

        <listitem>
          <para>2D matrix of the same size and type as Image</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function applies a filter for edge detection to an an image. No
    threshold for edge detection is calculated and no thresholding takes
    place. The filter result itself is returned.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">global IPD_PATH;

RGB = ReadImage(IPD_PATH + 'demos\teaset.png');

Image = RGB2Gray(RGB);

global EDGE_SOBEL;

Gradient = EdgeFilter(Image, EDGE_SOBEL);

figure(); ShowImage(Gradient, 'Gradient Image');</programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link
      linkend="DistanceTransform">DistanceTransform</link></member>

      <member><link linkend="Watershed">Watershed</link></member>

      <member><link linkend="SeparableFilter">SeparableFilter</link></member>

      <member><link linkend="MaskFilter">MaskFilter</link></member>
    </simplelist>
  </refsection>
</refentry>
