<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:xi="http://www.w3.org/2001/XInclude"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:html="http://www.w3.org/1999/xhtml"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv xml:id="MorphologicalFilter">
    <refname>MorphologicalFilter</refname>

    <refpurpose>applies a morphological filter to an image</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>FilteredImage = MorphologicalFilter(Image, FilterType, LogicalMatrix);</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>Image</term>

        <listitem>
          <para>2D matrix of type uint8, uint16, uint32 or double</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>FilterType</term>

        <listitem>
          <para>one of the constants FILTER_DILATE, FILTER_ERODE,
          FILTER_CLOSE, FILTER_OPEN, FILTER_TOP_HAT or FILTER_BOTTOM_HAT
          defined in loader.sce of IPD toolbox</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>LogicalMatrix</term>

        <listitem>
          <para>2D matrix of type boolean</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>FilteredImage</term>

        <listitem>
          <para>matrix of the same type and size as Image</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function applies a morphological filter to an image.
    DilateImage, ErodeImage, CloseImage, OpenImage, TopHat and BottomHat call
    this function so you do not need to call it yourself.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">Image = 0.5 * ones(9, 9) // generate gray image

Image(:, 3) = 0 // draw dark line

Image(:, 7) = 1 // draw light line

StructureElement = CreateStructureElement('square', 3) // generate structuring element

global FILTER_DILATE;

ResultImage = MorphologicalFilter(Image, FILTER_DILATE, StructureElement.Data)</programlisting>
  </refsection>

  <refsection>
    <title>See also</title>

    <simplelist type="inline">
      <member><link linkend="DilateImage">DilateImage</link></member>

      <member><link linkend="ErodeImage">ErodeImage</link></member>

      <member><link linkend="CloseImage">CloseImage</link></member>

      <member><link linkend="OpenImage">OpenImage</link></member>

      <member><link linkend="TopHat">TopHat</link></member>

      <member><link linkend="BottomHat">BottomHat</link></member>

      <member><link
      linkend="CreateStructureElement">CreateStructureElement</link></member>
    </simplelist>
  </refsection>
</refentry>
