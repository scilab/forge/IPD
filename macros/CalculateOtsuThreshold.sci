function Threshold = CalculateOtsuThreshold(Image)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
 
 // Global variables used as constants are declared.
 
 global TYPE_INT;
 
 global TYPE_DOUBLE;
 
 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs ~= 1 then
  
  error('Wrong number of input parameters.');
  
 end;

 CheckNumericMatrix(Image, 'Image');
 
 if NumberOfOutputs ~= 1 then
  
  error('Wrong number of output parameters.');
  
 end;
 
 // If Image is of type uint8, uint16 or uint32, it is converted to double.
 
 DataType = type(Image);
 
 if type(Image) == TYPE_INT then
 
  TypeName = typeof(Image);
  
  select TypeName
   
   case 'uint8'
    
    MaxGrayValue = 2 ^ 8 - 1;
    
   case 'uint16'

    MaxGrayValue = 2 ^ 16 - 1;
    
   case 'uint32'
    
    MaxGrayValue = 2 ^ 32 - 1;
    
  end;
  
  NormalizedImage = double(Image) / MaxGrayValue; 
     
 else
 
  NormalizedImage = Image;
  
 end;
  
 // A list of all unique gray values and their histogram are calculated.
 
 [Histogram ListOfGrayValues] = CreateHistogram(NormalizedImage);
 
 // The global mean value is calculated.
 
 GlobalMean = mean(NormalizedImage(:));
 
 // The histogram is normalized.
 
 NormalizedHistogram = Histogram / length(Image);
 
 // The cumulated histogram of the group of gray values lower than threshold
 // (dark) and the possible mean values are calculated.
 
 CumulatedHistogramDark = cumsum(NormalizedHistogram(1 : $ - 1));
 
 DarkMeanValues =  cumsum(NormalizedHistogram(1 : $ - 1) .* ...
                          ListOfGrayValues(1 : $ - 1));
 
 // The cumulated histogram of the group of gray values higher than threshold
 // (light) and the possible mean values are calculated.
 
 CumulatedHistogramLight = 1 - CumulatedHistogramDark;
 
 NumberOfGrayValues = length(ListOfGrayValues);
 
 LightMeanValues = zeros(1, NumberOfGrayValues - 1);

 for n = 1 : NumberOfGrayValues - 1

  LightMeanValues(n) = ListOfGrayValues(n + 1 : NumberOfGrayValues)' ...
                     * NormalizedHistogram(n + 1 : NumberOfGrayValues);
  
 end;
 
 // The between classes variance is calculated.
 
 BetweenVariance =  CumulatedHistogramDark ...
                 .* (DarkMeanValues - GlobalMean) .^ 2 ...
                 +  CumulatedHistogramLight ...
                 .* (LightMeanValues - GlobalMean)' .^ 2;

 [MaxBetweenVariance MaxIndex] = max(BetweenVariance);
 
 if DataType == TYPE_DOUBLE then
  
  Threshold = ListOfGrayValues(MaxIndex + 1);
  
 else
 
  ThresholdAsDouble = MaxGrayValue * ListOfGrayValues(MaxIndex + 1);
  
  select TypeName
   
   case 'uint8'
    
    Threshold = uint8(ThresholdAsDouble);
    
   case 'uint16'
    
    Threshold = uint16(ThresholdAsDouble);
    
   case 'uint32' 
     
    Threshold = uint32(ThresholdAsDouble);
    
  end;
   
 end;
  
endfunction

