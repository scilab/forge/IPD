function TextureImage = CalculateTextureEnergy(Image, FilterType)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

// Global variables used as constants are declared.

global TEXTURE_EDGE_EDGE;

global TEXTURE_EDGE_LEVEL;

global TEXTURE_EDGE_RIPPLE;

global TEXTURE_EDGE_SPOT;

global TEXTURE_LEVEL_RIPPLE;

global TEXTURE_LEVEL_SPOT;

global TEXTURE_RIPPLE_RIPPLE;

global TEXTURE_RIPPLE_SPOT;

global TEXTURE_SPOT_SPOT;

global TYPE_DOUBLE;

global TYPE_INT;

// Input and output parameters are checked.

[NumberOfOutputs NumberOfInputs] = argn();

if NumberOfInputs ~= 2 then
  
 error('Wrong number of input parameters.');
  
end;

CheckNumericMatrix(Image, 'Image');

if (length(FilterType) ~= 1) | (typeof(FilterType) ~= 'uint8') then
  
 error('FilterType must be a scalar of type uint8.');
  
end;

if NumberOfOutputs ~= 1 then
  
 error('Wrong number of output parameters.');
  
end;

// If Image is not of type double, it is converted to double.

select type(Image)
  
 case TYPE_DOUBLE
  
  ImageAsDouble = Image;
  
 case TYPE_INT
  
  select typeof(Image)
    
   case 'uint8'
     
    MaxGrayValue = 2 ^ 8 - 1; 
      
   case 'uint16'
     
    MaxGrayValue = 2 ^ 16 - 1; 
        
   case 'uint32'
     
    MaxGrayValue = 2 ^ 32 - 1; 
          
   else
     
    error('Image must be of type uint8, uint16, uint32 or double.');
        
  end;
  
  ImageAsDouble = double(Image) / MaxGrayValue;
  
end;

// The column and row filters are generated.

select FilterType
  
 case TEXTURE_EDGE_EDGE
   
  ColumnFilter = [-1; -2; 0; 2; 1];
   
  RowFilter = [-1, -2, 0, 2, 1];  
     
 case TEXTURE_EDGE_LEVEL
   
  ColumnFilter = [-1; -2; 0; 2; 1];  
  
  RowFilter = [1, 4, 6, 4, 1];
   
 case TEXTURE_EDGE_RIPPLE
   
  ColumnFilter = [-1; -2; 0; 2; 1];  
  
  RowFilter = [1, -4, 6, -4, 1];
    
 case TEXTURE_EDGE_SPOT
   
  ColumnFilter = [-1; -2; 0; 2; 1];   
    
  RowFilter = [-1, 0, 2, 0, -1];  
     
 case TEXTURE_LEVEL_RIPPLE
   
   ColumnFilter = [1; 4; 6; 4; 1];
   
   RowFilter = [1, -4, 6, -4, 1]; 
   
 case TEXTURE_LEVEL_SPOT
   
  ColumnFilter = [1; 4; 6; 4; 1]; 
  
  RowFilter = [-1, 0, 2, 0, -1];
    
 case TEXTURE_RIPPLE_RIPPLE
   
  ColumnFilter = [1; -4; 6; -4; 1];
   
  RowFilter = [1, -4, 6, -4, 1];
     
 case TEXTURE_RIPPLE_SPOT
   
  ColumnFilter = [1; -4; 6; -4; 1];
    
  RowFilter = [-1, 0, 2, 0, -1];
     
 case TEXTURE_SPOT_SPOT
   
  ColumnFilter = [-1; 0; 2; 0; -1];
   
  RowFilter = [-1, 0, 2, 0, -1]; 
   
 else
   
  error('Unknown texture filter.');  
   
end;

// The image is filtered with the column filter and the row filter. Then the
// sum of absolute gray values in a neighborhood is calculated.

FirstPass = abs(SeparableFilter(ImageAsDouble, ColumnFilter, RowFilter));

WindowWidth = 7;

WindowColumnFilter = ones(WindowWidth, 1);

WindowRowFilter = WindowColumnFilter';

FirstPass = SeparableFilter(FirstPass, WindowColumnFilter, WindowRowFilter);
                            
// The image is filtered again, but the filter sequence is reversed. Then the
// sum of absolute gray values in a neighborhood is calculated.

SecondPass = abs(SeparableFilter(ImageAsDouble, RowFilter, ColumnFilter));

SecondPass = SeparableFilter(SecondPass,  WindowColumnFilter, WindowRowFilter);

// The average of the first and second filteration is calculated and returned.

TextureImage = (FirstPass + SecondPass) / 2;
   
endfunction
