function CallbackMouseMove(WindowNumber, x, y, Button)
/////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////

  // Global variables used as constants are declared.

 global TYPE_INT;

 global TYPE_DOUBLE;

 global TYPE_BOOLEAN;

 MOUSE_MOVE = -1;
 
 WINDOW_CLOSED = -1000;
 
 // The mouse can be moved over the window or the window can be closed.
 
 select Button
 
  case MOUSE_MOVE
    
   // Further intializations
 
   FigureHandle = findobj('figure_id', WindowNumber);

   Row = y + 1;

   Column = x + 1;
 
   // Gray value or color text format is determined.
 
   select FigureHandle.user_data.ImageType
  
    case TYPE_INT

     Format = '%d';
  
    case TYPE_DOUBLE

     Format = '%f';
   
    case TYPE_BOOLEAN 
   
     Format = '%d';
   
   end;
 
   IsColor = (length(FigureHandle.user_data.ImageSize) > 2);
 
   if IsColor then 
     
    Format = '[' + Format + ', ' + Format + ', ' + Format + ']';
  
   end;
 
   // Language dependent part of info text is generated.

   select getlanguage()

    case 'de_DE'

     if IsColor then
    
      Text = 'aktuelles Pixel (%d, %d), Farbe = ' + Format;
     
     else
    
      Text = 'aktuelles Pixel (%d, %d), Grauwert ' + Format;
    
     end;
   
     if ~isempty(FigureHandle.user_data.SelectedPixel) then
    
      Text = Text + ', ausgewähltes Pixel (%d, %d)';

     end;

    case 'en_US'

     if IsColor then
      
      Text = 'current pixel (%d, %d), color ' + Format;
   
     else
    
      Text = 'current pixel (%d, %d), gray value ' + Format;
     
     end;

     if ~isempty(FigureHandle.user_data.SelectedPixel) then
    
      Text = Text + ', selected pixel (%d, %d)';

     end;

    case 'ja_JP'
    
     if IsColor then
      
      Text = '現在の画素（%d, %d)、色 ' + Format;
   
     else
    
      Text = '現在の画素（%d, %d)、グレイ地 ' + Format;
     
     end;
   
     if ~isempty(FigureHandle.user_data.SelectedPixel) then
    
      Text = Text + '、選択された画素（%d, %d)';

     end;

    else

     if IsColor then

      Text = 'current pixel (%d, %d), color ' + Format;

     else

      Text = 'current pixel (%d, %d), gray value ' + Format;

     end;

     if ~isempty(FigureHandle.user_data.SelectedPixel) then

      Text = Text + ', selected pixel (%d, %d)';

     end;

   end;

   // Info text is displayed.
 
   if IsColor then

    Color = FigureHandle.user_data.Image(Row, Column, :);

    if ~isempty(FigureHandle.user_data.SelectedPixel) then
      
     xinfo(msprintf(Text, ...
                    Column, ...
                    Row, ...
                    Color(1), ...
                    Color(2), ...
                    Color(3), ...
                    FigureHandle.user_data.SelectedPixel(1), ...
                    FigureHandle.user_data.SelectedPixel(2)));

    else

     xinfo(msprintf(Text, Column, Row, Color(1), Color(2), Color(3)));

    end;

   else

    if FigureHandle.user_data.ImageType ~= TYPE_BOOLEAN then

     GrayValue = FigureHandle.user_data.Image(Row, Column);

    else

     GrayValue = uint8(FigureHandle.user_data.Image(Row, Column));

    end;

    if ~isempty(FigureHandle.user_data.SelectedPixel) then

     xinfo(msprintf(Text, ...
                    Column, ...
                    Row, ...
                    GrayValue, ...
                    FigureHandle.user_data.SelectedPixel(1), ...
                    FigureHandle.user_data.SelectedPixel(2)));

    else

     xinfo(msprintf(Text, Column, Row, GrayValue));

    end;

   end;

  else
  
   return;
   
 end;
 
endfunction