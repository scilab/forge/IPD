function CallbackSelectedLine(ItemIndex, WindowNumber)
/////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////


 function Text = LineAsString()

  select getlanguage()

   case 'de_DE'

    Text = msprintf('Linie von (%d, %d) nach (%d, %d)', ...
                    UserData.StartPointLine(1), ...
                    UserData.StartPointLine(2), ...
                    UserData.EndPointLine(1), ...
                    UserData.EndPointLine(2));

   case 'en_US'

    Text = msprintf('Line from (%d, %d) to (%d, %d)', ...
                    UserData.StartPointLine(1), ...
                    UserData.StartPointLine(2), ...
                    UserData.EndPointLine(1), ...
                    UserData.EndPointLine(2));

   case 'ja_JP'

    Text = msprintf('(%d, %d)から(%d, %d)までの線', ...
                    UserData.StartPointLine(1), ...
                    UserData.StartPointLine(2), ...
                    UserData.EndPointLine(1), ...
                    UserData.EndPointLine(2));

   else

    Text = msprintf('Line from (%d, %d) to (%d, %d)', ...
                    UserData.StartPointLine(1), ...
                    UserData.StartPointLine(2), ...
                    UserData.EndPointLine(1), ...
                    UserData.EndPointLine(2));

  end;

 endfunction


 function Line = ExtractLine()

  Difference = UserData.EndPointLine - UserData.StartPointLine;

  NumberOfPixels = round(norm(Difference)) + 1;

  Step = Difference / (NumberOfPixels - 1);

  select length(FigureHandle.user_data.ImageSize)

   case 2

    NumberOfChannels = 1;

   case 3

    NumberOfChannels = FigureHandle.user_data.ImageSize(3);

   else

    error('An image must be a 2D or 3D matrix.');

  end;

  Line = zeros(NumberOfChannels, NumberOfPixels);

  if NumberOfChannels == 1 then

   Line(1) = UserData.Image(UserData.StartPointLine(2), ...
                            UserData.StartPointLine(1));

   Point = UserData.StartPointLine + Step;

   for n = 2 : NumberOfPixels - 1

    Line(n) = UserData.Image(Point(2), Point(1));

    Point = Point + Step;

   end;

   Line($) = UserData.Image(UserData.EndPointLine(2), UserData.EndPointLine(1));

  else

   Line(:, 1) = matrix(UserData.Image(UserData.StartPointLine(2), ...
                                      UserData.StartPointLine(1), ...
                                      :), ...
                       NumberOfChannels, ...
                       1);

   Point = UserData.StartPointLine + Step;

   for n = 2 : NumberOfPixels - 1

    Line(:, n) = matrix(UserData.Image(Point(2), Point(1), :), ...
                        NumberOfChannels, ...
                        1);

    Point = Point + Step;

   end;

   Line(:, $) = matrix(UserData.Image(UserData.EndPointLine(2), ...
                                      UserData.EndPointLine(1), ...
                                      :), ...
                       NumberOfChannels, ...
                       1);

  end;

 endfunction


 // Constants describing menu items and mouse events are generated.

 SELECT = 1;

 PROFILE = 2;

 HISTOGRAM = 3;

 EXPORT = 4;

 CANCEL = 5;

 NUMBER_OF_ITEMS = length([SELECT, PROFILE, HISTOGRAM, EXPORT, CANCEL]);

 select getlanguage()
  
  case 'de_DE'
   
   MENU_NAME = 'Linie';
    
  case 'en_US'
   
   MENU_NAME = 'Line';
    
  case 'ja_JP'
   
   MENU_NAME = '線';
    
  else
   
   MENU_NAME = 'Line';
   
 end;

 MOUSE_MOVE = -1;

 MOUSE_LEFT_CLICK = 3;

 // Further initializations.

 FigureHandle = findobj('figure_id', WindowNumber);

 scf(FigureHandle);

 UserData = FigureHandle.user_data;

 // Action is carried out.

 select ItemIndex

  case SELECT

   set(FigureHandle, 'event_handler_enable', 'off');

   x = zeros(1, 2);

   y = zeros(1, 2);

   for n = 1 : 2

    Button = MOUSE_MOVE;

    while Button ~= MOUSE_LEFT_CLICK

     [Button x(n) y(n)] = xclick();

    end;

   end;

   UserData.StartPointLine = ...
   [ceil(x(1) + 1) ceil(FigureHandle.user_data.ImageSize(1) - y(1) + 1)];

   UserData.EndPointLine = ...
   [ceil(x(2) + 1) ceil(FigureHandle.user_data.ImageSize(1) - y(2) + 1)];

   xpoly([x(1) x(2)], [y(1) y(2)]);

   UserData.SelectedLine = gce();

   xinfo(LineAsString());

   unsetmenu(WindowNumber, MENU_NAME, 1);

   for n = 2 : NUMBER_OF_ITEMS

    setmenu(WindowNumber, MENU_NAME, n);
 
   end;

   set(FigureHandle, 'event_handler_enable', 'on');

  case PROFILE

   Line = ExtractLine();

   UserData.ListOfDiagrams($ + 1) = ShowImageProfile(0 : size(Line, 2) - 1, ...
                                                     Line, ...
                                                     LineAsString());

  case HISTOGRAM

   UserData.ListOfDiagrams($ + 1) = ShowLineHistogram(ExtractLine(), ...
                                                      LineAsString());

  case EXPORT

   select getlanguage(); 

    case 'de_DE'

     Title = 'Export der ausgewählten Linie als globale Variable';

     Label = 'Name';

    case 'en_US'

     Title = 'Export of Selected Line as Global Variable';

     Label = 'Name';

    case 'ja_JP'

     Title = 'グローバル変数としてエキスポートされる線';

     Label = '名前';

    else

     Title = 'Export of Selected Line as Global Variable';

     Label = 'Name';

   end;

   ExportAsGlobalVariable(ExtractLine(), Title, Label, LineAsString());

  case CANCEL

   delete(UserData.SelectedLine);

   UserData.StartPointLine = [];

   UserData.EndPointLine = [];

   setmenu(WindowNumber, MENU_NAME, 1);

   for m = 2 : NUMBER_OF_ITEMS

    unsetmenu(WindowNumber, MENU_NAME, m);

   end;

 end;

 set(FigureHandle, 'user_data', UserData);

endfunction