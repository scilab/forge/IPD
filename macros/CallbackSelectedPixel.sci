function CallbackSelectedPixel(ItemIndex, WindowNumber)
/////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////


 function [x, y] = ExtractLine(SelectedPart, IsRow)

  if IsRow then

   x = 1 : FigureHandle.user_data.ImageSize(2);

  else

   x = 1 : FigureHandle.user_data.ImageSize(1);

  end;
  
  select length(FigureHandle.user_data.ImageSize)

   case 2

    NumberOfChannels = 1;

   case 3

    NumberOfChannels = FigureHandle.user_data.ImageSize(3);

   else

    error('An image must be a 2D or 3D matrix.');

  end;

  if NumberOfChannels == 1 then

   y = zeros(1, length(x));

   if IsRow then

    y(:) = FigureHandle.user_data.Image(SelectedPart, :);

   else

    for n = 1 : length(y)

     y(n) = FigureHandle.user_data.Image(n, SelectedPart);
 
    end

   end;

  else

   y = zeros(NumberOfChannels, length(x));

   for n = 1 : NumberOfChannels

    if IsRow then

     y(n, :) = FigureHandle.user_data.Image(SelectedPart, :, n);

    else

     y(n, :) = FigureHandle.user_data.Image(:, SelectedPart, n)';

    end;

   end;

  end;

 endfunction


 // Constants describing menu items are generated.
 
 SELECT = 1;
 
 ROW_PROFILE = 2;
 
 ROW_HISTOGRAM = 3;
 
 COLUMN_PROFILE = 4;
 
 COLUMN_HISTOGRAM = 5;
 
 EXPORT = 6;
 
 CANCEL = 7;
 
 NUMBER_OF_ITEMS = length([SELECT, ...
                           ROW_PROFILE, ...
                           ROW_HISTOGRAM, ...
                           COLUMN_PROFILE, ...
                           COLUMN_HISTOGRAM, ...
                           EXPORT, ...
                           CANCEL]);
 
 select getlanguage()
  
  case 'de_DE'
   
   MENU_NAME = 'Pixel';
    
  case 'en_US'
   
   MENU_NAME = 'Pixel';
    
  case 'ja_JP'
   
   MENU_NAME = '画素';
    
  else
   
   MENU_NAME = 'Pixel';
   
 end;
 
 // Constants for mouse events are generated.
 
 MOUSE_MOVE = -1;
 
 MOUSE_LEFT_CLICK = 3;

 // Further initializations.

 FigureHandle = findobj('figure_id', WindowNumber);
 
 scf(FigureHandle);

 UserData = FigureHandle.user_data;

 // Action is carried out.

 select ItemIndex
     
  case SELECT

   set(FigureHandle, 'event_handler_enable', 'off');

   Button = MOUSE_MOVE;

   while Button ~= MOUSE_LEFT_CLICK

    [Button x y] = xclick();

   end;

   x = ceil(x + 1);

   y = ceil(FigureHandle.user_data.ImageSize(1) - y + 1);

   select getlanguage()

    case 'de_DE'

     Text = 'ausgewähltes Pixel (%d, %d)';

    case 'en_US'

     Text = 'selected pixel (%d, %d)';

    case 'ja_JP'

     Text = '選択された画素（%d, %d)';

    else

     Text = 'selected pixel (%d, %d)';

   end;

   xinfo(msprintf(Text, x, y));

   UserData.SelectedPixel = [x y];

   unsetmenu(WindowNumber, MENU_NAME, 1);

   for m = 2 : NUMBER_OF_ITEMS

    setmenu(WindowNumber, MENU_NAME, m);

   end;

   set(FigureHandle, 'event_handler_enable', 'on');

  case ROW_PROFILE

   [XAxis YAxis] = ExtractLine(UserData.SelectedPixel(2), %t);

   select getlanguage()

    case 'de_DE'

     Title = 'Profil auf Zeile ' + string(UserData.SelectedPixel(2));

    case 'en_US'

     Title = 'Profile at Row ' + string(UserData.SelectedPixel(2));

    case 'ja_JP'

      Title = '行' + string(UserData.SelectedPixel(2)) + 'におけるプロファイル';

    else

     Title = 'Profile at Row ' + string(UserData.SelectedPixel(2));

   end;
   
   UserData.ListOfDiagrams($ + 1) = ShowImageProfile(XAxis, YAxis, Title);

  case ROW_HISTOGRAM

   [XAxis YAxis] = ExtractLine(UserData.SelectedPixel(2), %t);

   select getlanguage()

    case 'de_DE'

     Title = 'Histogramm auf Zeile ' + string(UserData.SelectedPixel(2));

    case 'en_US'

     Title = 'Histogram at Row ' + string(UserData.SelectedPixel(2));

    case 'ja_JP'

      Title = '行' ...
            + string(UserData.SelectedPixel(2)) ...
            + 'におけるヒストグラム';

    else

     Title = 'Histogram at Row ' + string(UserData.SelectedPixel(2));

   end;
   
   UserData.ListOfDiagrams($ + 1) = ShowLineHistogram(YAxis, Title);

  case COLUMN_PROFILE

   [XAxis YAxis] = ExtractLine(UserData.SelectedPixel(1), %f);

   select getlanguage()

    case 'de_DE'

     Title = 'Profil auf Spalte ' + string(UserData.SelectedPixel(1));

    case 'en_US'

     Title = 'Profile at Column ' + string(UserData.SelectedPixel(1));

    case 'ja_JP'

      Title = '列' + string(UserData.SelectedPixel(1)) + 'におけるプロファイル';

    else

     Title = 'Profile at Column ' + string(UserData.SelectedPixel(1));

   end;

   UserData.ListOfDiagrams($ + 1) = ShowImageProfile(XAxis, YAxis, Title);

  case COLUMN_HISTOGRAM

   [XAxis YAxis] = ExtractLine(UserData.SelectedPixel(1), %f);

   select getlanguage()

    case 'de_DE'

     Title = 'Histogramm auf Spalte ' + string(UserData.SelectedPixel(1));

    case 'en_US'

     Title = 'Histogram at Column ' + string(UserData.SelectedPixel(1));

    case 'ja_JP'

      Title = '列' ...
            + string(UserData.SelectedPixel(1)) ...
            + 'におけるヒストグラム';

    else

     Title = 'Histogram at Column ' + string(UserData.SelectedPixel(1));

   end;

   UserData.ListOfDiagrams($ + 1) = ShowLineHistogram(YAxis, Title);

  case EXPORT

   select getlanguage(); 

    case 'de_DE'

     Title = 'Export des ausgewählten Pixels als globale Variable';

     Label = 'Name';

    case 'en_US'

     Title = 'Export of Selected Pixel as Global Variable';

     Label = 'Name';

    case 'ja_JP'

     Title = 'グローバル変数としてエキスポートされる画素';

     Label = '名前';

    else

     Title = 'Export of Selected Pixel as Global Variable';

     Label = 'Name';

   end;

   Message = '[' ...
           + string(FigureHandle.user_data.SelectedPixel(1)) ...
           + ', ' ...
           + string(FigureHandle.user_data.SelectedPixel(2)) ...
           + ']';

   ExportAsGlobalVariable(FigureHandle.user_data.SelectedPixel, ...
                          Title, ...
                          Label, ...
                          Message);

  case CANCEL

   UserData.SelectedPixel = [];

   setmenu(WindowNumber, MENU_NAME, 1);
   
   for m = 2 : NUMBER_OF_ITEMS

    unsetmenu(WindowNumber, MENU_NAME, m);

   end;
 
 end;

 set(FigureHandle, 'user_data', UserData);
 
endfunction