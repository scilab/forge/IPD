function CallbackSelectedROI(ItemIndex, WindowNumber)
/////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////


 function Text = RectangleAsString()

  select getlanguage()

   case 'de_DE'

    Text = msprintf('Ausgewählter Bereich: %d <= x <= %d, %d <= y <= %d', ...
                    UserData.StartPointROI(1), ...
                    UserData.EndPointROI(1), ...
                    UserData.StartPointROI(2), ...
                    UserData.EndPointROI(2));

   case 'en_US'

    Text = msprintf('Selected area: %d <= x <= %d, %d <= y <= %d', ...
                    UserData.StartPointROI(1), ...
                    UserData.EndPointROI(1), ...
                    UserData.StartPointROI(2), ...
                    UserData.EndPointROI(2));

   case 'ja_JP'

    Text = msprintf('選択された領域：%d <= x <= %d、%d <= y <= %d', ...
                    UserData.StartPointROI(1), ...
                    UserData.EndPointROI(1), ...
                    UserData.StartPointROI(2), ...
                    UserData.EndPointROI(2));

   else

    Text = msprintf('Selected area: %d <= x <= %d, %d <= y <= %d', ...
                    UserData.StartPointROI(1), ...
                    UserData.EndPointROI(1), ...
                    UserData.StartPointROI(2), ...
                    UserData.EndPointROI(2));

  end;

 endfunction


 function SubImage = ExtractSubImage()

  select length(UserData.ImageSize)

   case 2

    SubImage = ...
    UserData.Image(UserData.StartPointROI(2) : UserData.EndPointROI(2), ...
                   UserData.StartPointROI(1) : UserData.EndPointROI(1));

   case 3

    SubImage = ...
    UserData.Image(UserData.StartPointROI(2) : UserData.EndPointROI(2), ...
                   UserData.StartPointROI(1) : UserData.EndPointROI(1), ...
                   :);

   else

    error('An image must be a 2D or 3D matrix.');

  end;

 endfunction


 // Constants describing menu items are generated.
 
 SELECT = 1;
 
 HISTOGRAM = 2;
 
 EXPORT = 3;
 
 CANCEL = 4;
 
 NUMBER_OF_ITEMS = length([SELECT, HISTOGRAM, EXPORT, CANCEL]);

 select getlanguage()

  case 'de_DE'

   MENU_NAME = 'Rechteckiger Bereich';

  case 'en_US'

   MENU_NAME = 'Rectangular Area';

  case 'ja_JP'

   MENU_NAME = '四角の領域';

  else

   MENU_NAME = 'Rectangular Area';

 end;

 // Further initializations.

 FigureHandle = findobj('figure_id', WindowNumber);
 
 scf(FigureHandle);

 UserData = FigureHandle.user_data;
 
 // Action is carried out.
 
 select ItemIndex
     
  case SELECT

   set(FigureHandle, 'event_handler_enable', 'off');

   Rectangle = rubberbox();
   
   xrect(Rectangle);

   UserData.SelectedRectangle = gce();

   // When a rectangle is extracted by rubberbox(), y grows from bottom to top
   // and starts at zero. Therefore, the second components must be mirrored for
   // matrix row calculation.

   Rectangle(2) = UserData.ImageSize(1) - Rectangle(2) + 1;

   // The result of rubberbox() usually consists of non integer numbers.

   UserData.StartPointROI = ceil(Rectangle);

   UserData.EndPointROI = floor(Rectangle(1 : 2) + Rectangle(3 : 4));

   // Only one rectangular area can be selected at a time.
   
   unsetmenu(WindowNumber, MENU_NAME, 1);
   
   for m = 2 : NUMBER_OF_ITEMS

    setmenu(WindowNumber, MENU_NAME, m);

   end;

   xinfo(RectangleAsString());

   set(FigureHandle, 'event_handler_enable', 'on');

  case HISTOGRAM

   UserData.ListOfDiagrams($ + 1) = ShowImageHistogram(ExtractSubImage(), ...
                                                       RectangleAsString());

  case EXPORT

   select getlanguage(); 

    case 'de_DE'

     Title = 'Export des ausgewählten Rechtecks als globale Variable';

     Label = 'Name';

    case 'en_US'

     Title = 'Export of Selected Rectangle as Global Variable';

     Label = 'Name';

    case 'ja_JP'

     Title = 'グローバル変数としてエキスポートされる四角の領域';

     Label = '名前';

    else

     Title = 'Export of Selected Rectangle as Global Variable';

     Label = 'Name';

   end;

   ExportAsGlobalVariable(ExtractSubImage(), Title, Label, RectangleAsString());

  case CANCEL

   delete(UserData.SelectedRectangle);

   UserData.StartPointROI = [];

   UserData.EndPointROI = [];

   setmenu(WindowNumber, MENU_NAME, 1);

   for m = 2 : NUMBER_OF_ITEMS

    unsetmenu(WindowNumber, MENU_NAME, m);

   end;
         
 end;

 set(FigureHandle, 'user_data', UserData);

endfunction