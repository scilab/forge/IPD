function CheckStructureElement(Input, Name)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 global TYPE_STRUCT;
 
 if (type(Input) ~= TYPE_STRUCT) ...
 |  (~isfield(Input, 'Width')) ...
 |  (~isfield(Input, 'Height')) ...
 |  (~isfield(Input, 'Data')) then
 
  error('Parameter ' + ... 
        Name + ...
        ' must be struct with the components ''Width'', ' + ...
        '''Height'' and ''Data''.');
   
 end;
 
endfunction

