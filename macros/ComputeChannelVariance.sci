function ChannelVariance = ComputeChannelVariance(MultiChannelImage, FilterSize)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

global TYPE_DOUBLE;

// Input and output parameters are checked.

[NumberOfOutputs NumberOfInputs] = argn();

if NumberOfInputs ~= 2 then
  
 error('Wrong number of input parameters.');
  
end;

// MultiChannelImage must be a 3D matrix of type double.

Dimensions = size(MultiChannelImage);

if length(Dimensions) ~= 3 then
  
 error('MultiChannelImage must be a 3D matrix of type double');
 
end;

if type(MultiChannelImage(1)) ~= TYPE_DOUBLE then
  
 error('MultiChannelImage must be a 3D matrix of type double');
 
end;

// FilterSize must be a vector.

if ~isvector(FilterSize) then
  
 error('FilterSize must be a vector with exactly two elements of type double.');
  
end;

if length(FilterSize) ~= 2 then
  
 error('FilterSize must be a vector with exactly two elements of type double.'); 
  
end;

if type(FilterSize(1)) ~= TYPE_DOUBLE then
  
 error('FilterSize must be a vector with exactly two elements of type double.');
  
end;

// There must be exactly one output argument.

if NumberOfOutputs ~= 1 then
 
 error('Wrong number of output parameters.');
 
end;

// Initialization
ChannelVariance = zeros(Dimensions(1), Dimensions(2), Dimensions(3));

// The variance is calculated for each channel now.

for n = 1 : Dimensions(3)
  
  ChannelVariance(:, :, n) = VarianceFilter(MultiChannelImage(:, :, n), ...
                                            FilterSize); 
  
end;

endfunction
