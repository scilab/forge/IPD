function IsCalculated = CreateFeatureStruct(InitialValue)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 // Global variables used as constants are declared.
 
 global TYPE_BOOLEAN;
 
 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs > 1 then
  
  error('Wrong number of inputs.');
  
 end;
 
 if NumberOfInputs == 1 then
  
  if (type(InitialValue) ~= TYPE_BOOLEAN) | (length(InitialValue) ~= 1) then   
  
   error('InitialValue must be boolean scalar.');
   
  end;
  
 else
  
  InitialValue = %f;
  
 end;
 
 if NumberOfOutputs ~= 1 then
  
  error('Wrong number of outputs.');
  
 end;
   
 IsCalculated = struct('PixelIndexList', ...
                       InitialValue, ...
                       'PixelList', ...
                       InitialValue, ...
                       'BoundingBox', ...
                       InitialValue, ...
                       'Centroid', ...
                       InitialValue);
                       
endfunction

