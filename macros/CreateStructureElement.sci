function StructureElement = CreateStructureElement(Name, Parameter)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
 
 // Input and output parameters are checked.

 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs ~= 2 then
  
  error('Wrong number of input parameters.');
  
 end;

 if NumberOfOutputs ~= 1 then
  
  error('Wrong number of output parameters.');
  
 end;
  
 CheckString(Name, 'Name');
 
 // Now the structuring element is generated.
 
 select convstr(Name, 'u')
  
  case 'CIRCLE'
   
   // A circular structure element is to be generated so Parameter must be
   // a scalar specifying its radius.
   
   CheckScalar(Parameter, 'Parameter');
   
   if Parameter < 1 then
    
    error('The radius of a circle must be at least one pixel.');
    
   end;
  
   Width = ceil(2 * Parameter + 1);
   
   Data = (zeros(Width, Width) > 0);
   
   Center = Parameter + 1;
   
   for n = 1 : Width
   
    for m = 1 : Width
    
     Data(m, n) = (norm([n - Center, m - Center]) <= Parameter);
     
    end;
    
   end;
      
   StructureElement = struct('Width', ...
                             Width, ...
                             'Height', ...
                             Width, ...
                             'Data', ...
                             Data);
     
  case 'VERTICAL_LINE'
   
   // The structure element shall be a vertical line so Parameter must be a
   // scalar specifying its length.
   
   CheckScalar(Parameter, 'Parameter');
   
   if (Parameter < 1) | (Parameter ~= round(Parameter)) then
    
    error('The length of a line must be an integer number greather ' + ...
          'than zero.');
    
   end;
  
   StructureElement = struct('Width', ...
                             1, ...
                             'Height', ...
                             Parameter, ...
                             'Data', ...
                             ones(Parameter, 1) > 0);
   
  case 'HORIZONTAL_LINE'
   
   // The structure element shall be a horizontal line so Parameter must be a
   // scalar specifying its length.
   
   CheckScalar(Parameter, 'Parameter');
   
   if (Parameter < 1) | (Parameter ~= round(Parameter)) then
    
    error('The length of a line must be an integer number greather ' + ...
          'than zero.');
    
   end;
  
   StructureElement = struct('Width', ...
                             Parameter, ...
                             'Height', ...
                             1, ...
                             'Data', ...
                             ones(1, Parameter) > 0);
     
  case 'RECTANGLE'
   
   // A rectangular structure element is to be created so Parameter must
   // be a vector specifying the number of rows in the first and the number
   // of columns in the second component.
   
   CheckVector(Parameter, 'Parameter');
   
   if length(Parameter) ~= 2 then
    
    error('Parameter must be a vector with exactly two components.');
    
   end;
   
   if (Parameter(1) < 1) ...
   |  (Parameter(1) ~= round(Parameter(1))) ...
   |  (Parameter(2) < 1) ...
   |  (Parameter(2) ~= round(Parameter(2))) then
   
    error('The height and width of a rectangle must be integer numbers ' + ...
          'greater than zero.');
          
   end;
   
   StructureElement = struct('Width', ...
                             Parameter(2), ...
                             'Height', ...
                             Parameter(1), ...
                             'Data', ...
                             ones(Parameter(1), Parameter(2)) > 0);
   
  case 'SQUARE'
 
   // A square structure element is to be generated so Parameter must be a
   // scalar specifying its width.
   
   CheckScalar(Parameter, 'Parameter');
   
   if (Parameter < 1) | (Parameter ~= round(Parameter)) then
    
    error('The width of a square must be an integer number greather than zeros.');
    
   end;
        
   StructureElement = struct('Width', ...
                             Parameter, ...
                             'Height', ...
                             Parameter, ...
                             'Data', ...
                             ones(Parameter, Parameter) > 0);

   case 'CUSTOM'
    
    // In this case the second input parameter is a boolean matrix and not a
    // vector that specifies the Parameter.
    
    CheckBooleanMatrix(Parameter, 'Parameter');
    
    StructureElement = struct('Width', ...
                              size(Parameter, 2), ...
                              'Height', ...
                              size(Parameter, 1), ...
                              'Data', ...
                              Parameter);
    
  else
   
   error('Parameter Name must be ''circle'', ''vertical_line'', ' + ...
         '''horizontal_line'', ''rectangle'', ''square'', or ' + ...
         '''custom''.');
   
 end;
   
endfunction

