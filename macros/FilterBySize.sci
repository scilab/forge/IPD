function FilteredImage = FilterBySize(Image, MinArea, MaxArea)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
 
 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 select NumberOfInputs
  
  case 2
   
   // If MaxArea is not specified, an object can have any size >= MinArea.
   // Therefore, MaxArea is set to infinite.
   
   MaxArea = %inf;
   
  case 3
   
   CheckScalar(MinArea, 'MinArea');
 
   CheckScalar(MaxArea, 'MaxArea');
 
   if MinArea > MaxArea then 
  
    error('MinArea must be smaller than or equal to MaxArea.');
  
   end;
    
  else
   
   error('Wrong number of input parameters.');
   
 end;
  
 CheckMatrix(Image, 'Image');
 
 if (typeof(Image) ~= 'uint32') & (typeof(Image) ~= 'int32') then
  
  error('Image must be a uint32 or an int32 matrix.');
  
 end;

 if NumberOfOutputs ~= 1 then
  
  error('Wrong number of output parameters.');
  
 end;
  
 // FilteredImage is initialized.
 
 [NumberOfRows NumberOfColumns] = size(Image);
 
 FilteredImage = uint32(zeros(NumberOfRows, NumberOfColumns));
 
 // The size of each object is determined.
 
 SizeHistogram = CreateHistogram(Image);
 
 // All objects with MinArea <= number of pixels <= MaxArea are transferred to
 // FilteredImage.
  
 ObjectIndex = uint32(1);
 
 NumberOfObjects = length(SizeHistogram) - 1;

 for n = 1 : NumberOfObjects
  
  Area = SizeHistogram(n + 1);
  
  if (Area >= MinArea) & (Area <= MaxArea) then
   
   FilteredImage(Image == n) = ObjectIndex;
   
   ObjectIndex = ObjectIndex + 1;
   
  end;

 end;
  
endfunction

