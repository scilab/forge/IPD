function VideoStruct = GetVideoStruct(VideoPath)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs ~= 1 then
   
  error('Wrong number of input parameters.');
   
 end;
 
 CheckString(VideoPath, 'VideoPath');
 
 if NumberOfOutputs ~= 1 then
   
  error('Wrong number of output parameters.');
   
 end;
 
 VideoList = GetVideoInfo(VideoPath);
 
 VideoStruct = struct(VideoList(:));
 
endfunction