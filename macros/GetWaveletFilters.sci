function FilterStruct = GetWaveletFilters(WaveletType)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

global WAVELET_DAUBECHIES_2;

global WAVELET_DAUBECHIES_4;

global WAVELET_DAUBECHIES_6;

global WAVELET_DAUBECHIES_8;

global WAVELET_DAUBECHIES_10;

global WAVELET_DAUBECHIES_12;

global WAVELET_DAUBECHIES_14;

global WAVELET_DAUBECHIES_16;

global WAVELET_DAUBECHIES_18;

global WAVELET_DAUBECHIES_20;

global WAVELET_COIFLET_6;

global WAVELET_COIFLET_12;

global WAVELET_COIFLET_18;

global WAVELET_COIFLET_24;

global WAVELET_COIFLET_30;

global WAVELET_CDF;

global WAVELET_PATH;

// Input and output parameters are checked.

[NumberOfOutputs NumberOfInputs] = argn();

if NumberOfInputs ~= 1 then
  
 error('Wrong number of input parameters.');
  
end;

CheckScalar(WaveletType, 'WaveletType');

if NumberOfOutputs ~= 1 then
  
 error('Wrong number of output parameters.');
  
end;

// The file name and the column of the low pass filter are determined.

select WaveletType
  
 case WAVELET_DAUBECHIES_2
   
  Path = WAVELET_PATH + 'Daubechies.csv';
  
  Columns = 1 : 2;
   
 case WAVELET_DAUBECHIES_4
   
  Path = WAVELET_PATH + 'Daubechies.csv';
  
  Columns = 3 : 4;
   
 case WAVELET_DAUBECHIES_6
   
  Path = WAVELET_PATH + 'Daubechies.csv';
  
  Columns = 5 : 6;
   
 case WAVELET_DAUBECHIES_8
   
  Path = WAVELET_PATH + 'Daubechies.csv';
  
  Columns = 7 : 8;
   
 case WAVELET_DAUBECHIES_10
   
  Path = WAVELET_PATH + 'Daubechies.csv';
  
  Columns = 9 : 10;
   
 case WAVELET_DAUBECHIES_12
   
  Path = WAVELET_PATH + 'Daubechies.csv';
  
  Columns = 11 : 12;
   
 case WAVELET_DAUBECHIES_14
   
  Path = WAVELET_PATH + 'Daubechies.csv';
  
  Columns = 13 : 14;
   
 case WAVELET_DAUBECHIES_16
   
  Path = WAVELET_PATH + 'Daubechies.csv';
  
  Columns = 15 : 16;
   
 case WAVELET_DAUBECHIES_18
   
  Path = WAVELET_PATH + 'Daubechies.csv';
  
  Columns = 17 : 18;
   
 case WAVELET_DAUBECHIES_20
   
  Path = WAVELET_PATH + 'Daubechies.csv';
  
  Columns = 19 : 20;
   
 case WAVELET_COIFLET_6
   
  Path = WAVELET_PATH + 'Coiflet.csv';
  
  Columns = 1 : 2;
   
 case WAVELET_COIFLET_12
   
  Path = WAVELET_PATH + 'Coiflet.csv';
  
  Columns = 3 : 4; 
   
 case WAVELET_COIFLET_18
   
  Path = WAVELET_PATH + 'Coiflet.csv';
  
  Columns = 5 : 6; 
   
 case WAVELET_COIFLET_24
   
  Path = WAVELET_PATH + 'Coiflet.csv';
  
  Columns = 7 : 8; 
   
 case WAVELET_COIFLET_30
   
  Path = WAVELET_PATH + 'Coiflet.csv';
  
  Columns = 9 : 10; 
   
 case WAVELET_CDF
   
  Path = WAVELET_PATH + 'CDF.csv';
  
  Columns = 1 : 2;

 else

  error('Unknown wavelet type.');

end;

// The csv file containing the wavelet coefficients is read. The number of
// rows is determined.

Coefficients = read_csv(Path, ascii(9));

Rows = find(Coefficients(:, Columns(1)) ~= 'Nan');

// The coefficients are extracted and converted from string to double.
SelectedCoefficients = strtod(Coefficients(Rows, Columns));

// The coefficients are returned as a struct.

FilterStruct = struct('LowPass', ...
                      SelectedCoefficients(:, 1), ...
                      'HighPass', ...
                      SelectedCoefficients(:, 2));

endfunction