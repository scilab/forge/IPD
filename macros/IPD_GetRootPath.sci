// Allan CORNET - 2012
function p = IPD_GetRootPath()
  p = [];
  if isdef('ipdlib') then
    [m, mp] = libraryinfo('ipdlib');
    p = pathconvert(fullpath(mp + "../"), %t, %t);
  end
endfunction
