function RGB = Ind2RGB(IndexImage, ColorMap, BackGroundColor)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 // Global variables used as constants are declared.
 
 global TYPE_INT;
 
 global TYPE_DOUBLE;
 
 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if (NumberOfInputs < 2) | (NumberOfInputs > 3) then
   
  error('Wrong number of input parameters.');
   
 end;
 
 if NumberOfOutputs ~= 1 then
   
  error('Wrong number of output parameters.');
   
 end;
 
 CheckMatrix(IndexImage, 'IndexImage');
 
 CheckMatrix(ColorMap, 'ColorMap');
 
 if size(ColorMap, 2) ~= 3 then
   
  error('ColorMap must be a matrix with exactly three columns.');
   
 end;
 
 if NumberOfInputs == 3 then
   
  CheckVector(BackGroundColor, 'BackGroundColor');
   
  if length(BackGroundColor) ~= 3 then
      
   error('BackGroundColor must be a vector with exactly three elements.');
   
  end;
  
  select type(BackGroundColor)
    
   case TYPE_DOUBLE
     
    if (BackGroundColor(1) < 0) | (BackGroundColor(1) > 1) ...
    |  (BackGroundColor(2) < 0) | (BackGroundColor(2) > 1) ...
    |  (BackGroundColor(3) < 0) | (BackGroundColor(3) > 1) then
    
     error('All elements of BackGroundColor must have values between 0 and 1.');
    
    end;
    
    BackGround = BackGroundColor;
    
   case TYPE_INT
     
    select typeof(BackGroundColor)
      
     case 'uint8'
       
      BackGround = double(BackGroundColor) / (2 ^ 8);
       
     case 'uint16'
       
      BackGround = double(BackGroundColor) / (2 ^ 16); 
       
     case 'uint32'
       
      BackGround = double(BackGroundColor) / (2 ^ 32); 
       
     else
       
      error('BackGroundColor must be of type uint8, uint16, uint32 or double.'); 
            
    end;
         
   else
     
    error('BackGroundColor must be of type uint8, uint16, uint32 or double.');
            
  end;

 end;
   
 // Initialization.
 
 Dimensions = size(IndexImage);
 
 NumberOfPixels = length(IndexImage);
 
 RGB = ones(Dimensions(1), Dimensions(2), 3);
 
 if NumberOfInputs == 3 then
   
  RGB(:, :, 1) = BackGround(1);
   
  RGB(:, :, 2) = BackGround(2);  
  
  RGB(:, :, 3) = BackGround(3);
  
 end;
  
 for n = 1 : NumberOfPixels
   
  Index = IndexImage(n);
  
  if Index > 0 then
      
   Color = ColorMap(Index, :);
      
   [Row Column] = ind2sub([Dimensions], n);
  
   RGB(Row, Column, :) = Color;   
      
  end;

 end;
 
endfunction
