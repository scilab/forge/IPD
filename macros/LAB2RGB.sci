function RGB = LAB2RGB(LAB)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

global TYPE_DOUBLE;

global CONVERSION_LAB2RGB;

// Input and output parameters are checked.
 
[NumberOfOutputs NumberOfInputs] = argn();
 
if NumberOfInputs ~= 1 then
   
 error('Wrong number of input parameters.');
   
end;
 
if ndims(LAB) ~= 3 then
   
 error('LAB must be a 3D matrix of type uint8 or double.');
   
end;
 
if NumberOfOutputs ~= 1 then
   
 error('Wrong number of output parameters.');
   
end;
 
// Only double images are supported.
 
if type(LAB(1)) == TYPE_DOUBLE then
   
 PixelList = ConvertColorSpace(LAB(:), size(LAB), CONVERSION_LAB2RGB);
    
 RGB = matrix(PixelList, size(LAB));
      
else
   
 error('LAB must be of type double.'); 
     
end;

endfunction
