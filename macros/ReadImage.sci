function RGB = ReadImage(ImageSource)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 // Global variables used as constants are declared.
 
 global TYPE_STRING;
 
 global TYPE_POINTER;
  
 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs ~= 1 then
  
  error('Wrong number of input parameters.');
  
 end;
 
 SourceType = type(ImageSource);
 
 if (SourceType ~= TYPE_STRING) & (SourceType ~= TYPE_POINTER) then
   
  error('SourceType must be a string or a pointer.');
   
 end;
  
 if NumberOfOutputs ~= 1 then 
  
  error('Wrong number of outputs.');
  
 end;
 
 // A list of pixel values and the dimensions (number of rows, number of columns 
 // and number of channels) of the image are retrieved.
 
 select SourceType
 
  case TYPE_STRING
    
   if ~isfile(ImageSource) then
   
    error(ImageSource + ' not found.');
    
   end;
   
   [PixelList Dimensions] = ReadImageFile(ImageSource); 
    
  case TYPE_POINTER
    
   [PixelList Dimensions] = ReadImageFromVideo(ImageSource);
       
 end;

 // If the image is a color image and therefore has more than one channel, RGB is a 
 // hypermatrix. If the image has only one channel, RGB is a 2D matrix.
 
 if Dimensions($) > 1 then
   
   RGB = matrix(PixelList, Dimensions);
   
 else
   
   RGB = matrix(PixelList, Dimensions(1 : $ - 1));
   
 end;
  
endfunction