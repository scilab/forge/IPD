function ScaledGrayValues = ScaleToUInt8(Image)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 MaxAllowedGrayValue = 2 ^ 8 - 1;
       
 MaxGrayValue = max(Image(:));
      
 if MaxGrayValue <= MaxAllowedGrayValue then
        
  PixelList = uint8(Image(:));
      
 else
       
  GrayValueList = double(Image(:));
       
  MinGrayValue = min(GrayValueList(GrayValueList > 0));
        
  if MaxGrayValue > MinGrayValue then
           
   GrayValueList = (  (MaxAllowedGrayValue - 1) * GrayValueList ...
                    - MinGrayValue * MaxAllowedGrayValue ...
                    + MaxGrayValue) ...
                  / (MaxGrayValue - MinGrayValue);
       
  else
        
   GrayValueList(GrayValueList > 0) = MaxAllowedGrayValue;
                        
  end;
      
  PixelList = uint8(GrayValueList);  
                 
 end;
    
endfunction
