function SegmentedImage = SegmentByThreshold(Image, Threshold)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
 
 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs ~= 2 then
  
  error('Wrong number of input parameters.');
  
 end;

 if NumberOfOutputs ~= 1 then
  
  error('Wrong number of output parameters.');
  
 end;
  
 CheckNumericMatrix(Image, 'Image');
 
 CheckScalar(Threshold, 'Threshold');
 
 // All pixels with a gray value >= Threshold are set to %t.
 SegmentedImage = (Image >= Threshold);
 
endfunction

