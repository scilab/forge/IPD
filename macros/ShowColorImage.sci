function FigureHandle = ShowColorImage(RGB, Title)
////////////////////////////////////////////////////////////////////////////  
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 // Global variables used as constants are declared.

 global TYPE_DOUBLE;
 
 global TYPE_INT;
 
 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs ~= 2 then
   
  error('Wrong number of input parameters.');
   
 end;
 
 // RGB must be a 3D matrix of type uint8, uint16, uint32 or double.
 
 if length(size(RGB)) ~= 3 then
   
  error('RGB must be a 3D matrix of type uint8, uint16, uint32 or double.');
   
 end;
 
 Sample = RGB(1);
 
 if (type(Sample) ~= TYPE_INT) & (type(Sample) ~= TYPE_DOUBLE) then
   
  error('RGB must be a 3D matrix of type uint8, uint16, uint32 or double.');
     
 end;
 
 // Title must be a string.
 CheckString(Title, 'Title');
 
 // Output parameters are checked.
 
 if NumberOfOutputs > 1 then
   
  error('Wrong number of output parameters.');
   
 end;
 
 // RGB is transformed to an indexed image.
 [IndexImage ColorMap] = RGB2Ind(RGB);
  
 // The indexed image is displayed.
 FigureHandle = ShowImage(IndexImage, Title, ColorMap);
  
endfunction