function DiagramHandle = ShowLineHistogram(ImageLine, Title)
/////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////

 DiagramHandle = figure('figure_name', Title, 'BackgroundColor', [1 1 1]);

 ImageSize = size(ImageLine);

 NumberOfChannels = ImageSize(1);

 select getlanguage()

  case 'de_DE'

   XLabel = 'Wert';

   YLabel = 'Häufigkeit';

   if NumberOfChannels > 1 then 

    ChannelName = 'Kanal';

   end;

  case 'en_US'

   XLabel = 'Value';

   YLabel = 'Frequency';

   if NumberOfChannels > 1 then 

    ChannelName = 'Channel';

   end;

  case 'ja_JP'

   XLabel = 'Value';

   YLabel = 'Frequency';

   if NumberOfChannels > 1 then 

    ChannelName = 'Channel';

   end;

  else

   XLabel = 'Value';

   YLabel = 'Frequency';

   if NumberOfChannels > 1 then 

    ChannelName = 'Channel';

   end;

 end;
 
 if NumberOfChannels == 1 then

  [Histogram ListOfBins] = CreateHistogram(ImageLine);
  
  plot(ListOfBins, Histogram);

  xtitle('', XLabel, YLabel);

  xgrid();

 else

  for n = 1 : NumberOfChannels 

   subplot(NumberOfChannels, 1, n);

   [Histogram ListOfBins] = CreateHistogram(ImageLine(n, :));
  
   plot(ListOfBins, Histogram);

   xtitle(ChannelName + ' ' + string(n), XLabel, YLabel);
   
   xgrid();

  end;

 end;

endfunction