IPD - Image Processing Design Toolbox Version 8.2
Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011.
Conveyed under the conditions of the GNU GPL Version 3 or later.


This toolbox provides functions for designing and parameterizing image
processing algorithms. The following functionality is provided:

1. Histogram calculation
   - gray level histogram
   - cumulated histogram

2. Thresholding
   - segmentation by threshold
   - threshold calculation by Otsu method

3. Morphological filters
   - dilation
   - erosion
   - closing
   - opening
   - top hat
   - bottom hat

4. Median filter

5. Filtering for edge detection
    - Sobel filter
    - Laplace filter
    - Prewitt filter
    - Scharr filter

6. Region based segmentation
   - distance transform
   - watershed transform

7. Blob analysis
   - searching connected regions in binary images
   - removing blobs that have less pixels than a lower bound or more pixels
     than an upper bound
   - calculating specific properties of blobs such as bounding box, centroid
   - drawing bounding boxes around objects found in an image
   - calculating histogram of blob sizes
   - calculating cumulated histogram of blob sizes

8. Displaying images in figure windows
   - display
   - conversion of RGB to indexed images
   - conversion of indexed images to RGB 

9. Conversion between color images and gray level images
   - RGB    to  gray level
   - RGB    to  L*a*b* 
   - L*a*b* to  RGB 

10. Opening image and video files
   - reading image files
   - writing image files
   - reading video files

11. Linear filteration

12. Variance filter

13. Texture feature calculation
   - Laws texture energy
   - discrete Wavelet frames

14. Template Matching

15. Interactive Image Analysis GUI


This version uses OpenCV 2.1 which is available for download at

     opencv.willowgarage.com/wiki/

If you intend to work with the file red-car-video.avi, please install the XVid
codec which is available for download at 

      www.xvid.org
