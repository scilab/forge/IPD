////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#include "ScilabToIplImage.h"
#include "IplImageToScilab.h"
#include "TransferImageData.h"

#include "gw_ipd.h"
#include "IPD.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};

int sci_ConvertColorSpace(char* fname)
{
    // Input and output parameters are checked.

    CheckRhs(3, 3);

    CheckLhs(1, 1);

    // The first input parameter is a column or row vector containing the color intensities. The
    // vector must be of data type uint8 or double.

    int* ImageAddress = NULL;

    SciErr ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &ImageAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int ImageType = 0;

    ScilabError = getVarType(pvApiCtx, ImageAddress, &ImageType);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int Precision = 0;

    int NumberOfRows = 0;

    int NumberOfColumns = 0;

    switch(ImageType)
    {
    case sci_matrix:
        {
            // Okay.
        } // case

        break;

    case sci_ints:
        {
            // Only uint8 and double can be processed by this function.

            ScilabError = getMatrixOfIntegerPrecision(pvApiCtx, ImageAddress, &Precision);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                return 0;
            } // if

            if(Precision != SCI_UINT8)
            {
                Scierror(ERROR_ARGUMENT, "The first argument must be a vector of type uint8 or double.");

                return 0;
            } // if
        } // case

        break;

    default:
        {
            Scierror(ERROR_ARGUMENT, "The first argument must be a vector of type uint8 or double.");

            return 0;
        } // default

        break;
    } // switch

    ScilabError = getVarDimension(pvApiCtx, ImageAddress, &NumberOfRows, &NumberOfColumns);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if((NumberOfRows != 1) && (NumberOfColumns != 1))
    {
        Scierror(ERROR_ARGUMENT, "The first argument must be a vector of type uint8 or double.");

        return 0;
    } // if

    // The second argument is a vector containing exactly three elements. The first is the number
    // of rows, the second is the number of columns, the third is the number of channels. As the
    // Scilab function size returns a vector of type double, the second argument must be of type
    // double, too.

    int* DimensionsAddress = NULL;

    ScilabError = getVarAddressFromPosition(pvApiCtx, 2, &DimensionsAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    double* Dimensions = 0;

    ScilabError = getMatrixOfDouble(pvApiCtx,
        DimensionsAddress,
        &NumberOfRows,
        &NumberOfColumns,
        &Dimensions);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if(!(((NumberOfRows == 3)  && (NumberOfColumns == 1)) ||
        ((NumberOfRows == 1) &&  (NumberOfColumns == 3))))
    {
        string Message1("The second argument must be a vector of type double with exactly three ");

        string Message2("elements.");

        string Message(Message1 + Message2);

        Scierror(ERROR_ARGUMENT, const_cast<char*>(Message.data()));

        return 0;
    } // if

    // The third argument is a scalar of type uint8 specifying the color space conversion.

    int* ConversionAddress = NULL;

    ScilabError = getVarAddressFromPosition(pvApiCtx, 3, &ConversionAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    TUChar* Conversion = NULL;

    ScilabError = getMatrixOfUnsignedInteger8(pvApiCtx,
        ConversionAddress,
        &NumberOfRows,
        &NumberOfColumns,
        &Conversion);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if((NumberOfRows != 1) || (NumberOfColumns != 1))
    {
        Scierror(ERROR_ARGUMENT, "The third argument must be a scalar of type uint8.");

        return 0;
    } // if

    // The color spaces of source and destination images are determined now.

    int Direction = 0;

    TUChar SourceColorSpace = COLOR_SPACE_RGB;

    TUChar DestinationColorSpace = COLOR_SPACE_RGB;

    switch(*Conversion)
    {
    case RGB2GRAY:
        {
            Direction = CV_BGR2GRAY;

            DestinationColorSpace = COLOR_SPACE_GRAY;
        } // case

        break;

    case RGB2LAB:
        {
            Direction = CV_BGR2Lab;

            DestinationColorSpace = COLOR_SPACE_LAB;
        } // case

        break;

    case LAB2RGB:
        {
            Direction = CV_Lab2BGR;

            SourceColorSpace = COLOR_SPACE_LAB;
        } // case

        break;

    default:
        {
            Scierror(ERROR_ARGUMENT, "Unknown color space conversion.");

            return 0;
        }// default

        break;
    } // switch

    // The source and destination images are generated.

    IplImage* Source = NULL;

    IplImage* Destination = NULL;

    switch(ImageType)
    {
    case sci_matrix:
        {
            double* ImageData;

            ScilabError = getMatrixOfDouble(pvApiCtx,
                ImageAddress,
                &NumberOfRows,
                &NumberOfColumns,
                &ImageData);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                return 0;
            } // if

            float* ImageDataAsFloat = NULL;

            TransferImageData(static_cast<int>(Dimensions[0] * Dimensions[1] * Dimensions[2]),
                ImageData,
                &ImageDataAsFloat);

            if(ImageDataAsFloat == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return 0;
            } // if

            ScilabToIplImage(&Source,
                ImageDataAsFloat,
                static_cast<TUInt>(Dimensions[1]),
                static_cast<TUInt>(Dimensions[0]),
                static_cast<TUInt>(Dimensions[2]),
                FLOAT_IMAGE,
                Precision,
                SourceColorSpace);

            delete ImageDataAsFloat;

            if(Source == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return 0;
            } // if

            int NumberOfChannels = static_cast<int>(Dimensions[2]);

            if(DestinationColorSpace == COLOR_SPACE_GRAY)
            {
                NumberOfChannels = 1;
            } // if

            Destination = cvCreateImage(cvSize(static_cast<int>(Dimensions[1]),
                static_cast<int>(Dimensions[0])),
                IPL_DEPTH_32F,
                NumberOfChannels);

            if(Destination == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                cvReleaseImage(&Source);

                return 0;
            } // if
        } // case

        break;

    case sci_ints:
        {
            TUChar* ImageData;

            ScilabError = getMatrixOfUnsignedInteger8(pvApiCtx,
                ImageAddress,
                &NumberOfRows,
                &NumberOfColumns,
                &ImageData);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                return 0;
            } // if

            ScilabToIplImage(&Source,
                ImageData,
                static_cast<TUInt>(Dimensions[1]),
                static_cast<TUInt>(Dimensions[0]),
                static_cast<TUInt>(Dimensions[2]),
                ImageType,
                Precision,
                SourceColorSpace);

            if(Source == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return 0;
            } // if

            int NumberOfChannels = static_cast<int>(Dimensions[2]);

            if(DestinationColorSpace == COLOR_SPACE_GRAY)
            {
                NumberOfChannels = 1;
            } // if

            Destination = cvCreateImage(cvSize(static_cast<int>(Dimensions[1]),
                static_cast<int>(Dimensions[0])),
                IPL_DEPTH_8U,
                NumberOfChannels);

            if(Destination == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                cvReleaseImage(&Source);

                return 0;
            } // if
        } // case

        break;

    default:
        {
        } // default

        break;
    } // switch

    // The color space conversion is performed now. The source image is not needed anymore.

    cvCvtColor(Source, Destination, Direction);

    cvReleaseImage(&Source);

    // Destination is transformed to a Scilab image and returned.

    switch(ImageType)
    {
    case sci_matrix:
        {
            float* ImageData = NULL;

            IplImageToScilab(Destination, &ImageData, DestinationColorSpace);

            if(ImageData == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                cvReleaseImage(&Destination);

                return 0;
            } // if

            int NumberOfElements = Destination->width * Destination->height * Destination->nChannels;

            double* ImageDataAsDouble = new double[NumberOfElements];

            if(ImageDataAsDouble == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                cvReleaseImage(&Destination);

                return 0;
            } // if

            TransferImageData(NumberOfElements, ImageData, &ImageDataAsDouble);

            if(ImageDataAsDouble == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                cvReleaseImage(&Destination);

                return 0;
            } // if

            ScilabError = createMatrixOfDouble(pvApiCtx,
                Rhs + 1,
                NumberOfElements,
                1,
                ImageDataAsDouble);

            cvReleaseImage(&Destination);

            delete(ImageData);

            if(ScilabError.iErr)
            {
                delete ImageDataAsDouble;

                printError(&ScilabError, 0);

                return 0;
            } // if
        } // case

        break;

    case sci_ints:
        {
            TUChar* ImageData = NULL;

            IplImageToScilab(Destination, &ImageData, DestinationColorSpace);

            if(ImageData == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                cvReleaseImage(&Destination);

                return 0;
            } // if

            int NumberOfElements = Destination->width * Destination->height * Destination->nChannels;

            ScilabError = createMatrixOfUnsignedInteger8(pvApiCtx,
                Rhs + 1,
                NumberOfElements,
                1,
                ImageData);

            cvReleaseImage(&Destination);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                return 0;
            } // if
        } // case

        break;

    default:
        {
        } // default

        break;
    } // switch

    // Position of output parameter is set.
    LhsVar(1) = Rhs + 1;

    return 0;
} // sci_ConvertColorSpace
