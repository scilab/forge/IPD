////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#include "gw_ipd.h"
#include "IPD.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};



int sci_GetVideoInfo(char*fname)
{
    // Input and output parameters are checked.

    CheckRhs(1, 1);

    CheckLhs(1, 1);

    // The input argument is the file name which must be a string.

    // Address of input parameter (file name) is determined.

    SciErr ScilabError;

    int* FileNamePointer = NULL;

    ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &FileNamePointer);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // Length of file name is determined. An input parameter of type string can be a matrix with
    // several rows and columns. Each element of this matrix is a string. Therefore, the number
    // of columns and rows are checked. A file name should consist of only one single string, so
    // the function returns otherwise.

    int NumberOfRows = 0;

    int NumberOfColumns = 0;

    int StringLength = 0;

    ScilabError = getMatrixOfString(pvApiCtx,
        FileNamePointer,
        &NumberOfRows,
        &NumberOfColumns,
        NULL,
        NULL);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if((NumberOfRows != 1) || (NumberOfColumns != 1))
    {
        Scierror(ERROR_FILE_NAME, "Invalid file name. A file name must be a string.");

        return 0;
    } // if

    // Length of file name is determined.

    ScilabError = getMatrixOfString(pvApiCtx,
        FileNamePointer,
        &NumberOfRows,
        &NumberOfColumns,
        &StringLength,
        NULL);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // File name is retrieved.

    char* FileName = new char[StringLength + 1];

    if(FileName == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    ScilabError = getMatrixOfString(pvApiCtx,
        FileNamePointer,
        &NumberOfRows,
        &NumberOfColumns,
        &StringLength,
        &FileName);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        delete FileName;

        return 0;
    } // if

    // The video file with the specified file name is opened.

    CvCapture* VideoFile = cvCreateFileCapture(FileName);

    delete FileName;

    if(VideoFile == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    // The codec is determined.

    double Property = cvGetCaptureProperty(VideoFile, CV_CAP_PROP_FOURCC);

    int CodecAsInt = static_cast<int>(Property);

    char* Codec = new char[CODEC_LENGTH + 1];

    if(Codec == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    memcpy(Codec, &CodecAsInt, CODEC_LENGTH * sizeof(char));

    Codec[CODEC_LENGTH] = char(0);

    // The number of frames is determined.
    double NumberOfFrames = cvGetCaptureProperty(VideoFile, CV_CAP_PROP_FRAME_COUNT);

    // The frame rate is determined.
    double FrameRate = cvGetCaptureProperty(VideoFile, CV_CAP_PROP_FPS);

    // The width of a frame is determined.
    double Width = cvGetCaptureProperty(VideoFile, CV_CAP_PROP_FRAME_WIDTH);

    // The height of a frame is determined.
    double Height = cvGetCaptureProperty(VideoFile, CV_CAP_PROP_FRAME_HEIGHT);

    // It is determined whether or not the video is color or not.

    IplImage* Image = cvQueryFrame(VideoFile);

    int IsColor = static_cast<int>(true);

    if(Image != NULL)
    {
        IsColor = static_cast<int>(Image->nChannels > 1);
    } // if

    // The video file is closed now.
    cvReleaseCapture(&VideoFile);

    // The output argument is a list with twelve items. Each item determined above is preceded by
    // a name so the meaning of items is easy to understand for the user.

    int* Address = NULL;

    ScilabError = createList(pvApiCtx, Rhs + 1, 12, &Address);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The first item is the codec.

    string ParameterName("Codec");

    char* NamePointer = const_cast<char*>(ParameterName.data());

    ScilabError = createMatrixOfStringInList(pvApiCtx, Rhs + 1, Address, 1, 1, 1, &NamePointer);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        delete Codec;

        return 0;
    } // if

    ScilabError = createMatrixOfStringInList(pvApiCtx, Rhs + 1, Address, 2, 1, 1, &Codec);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The second item is the number of frames.

    ParameterName = "NumberOfFrames";

    NamePointer = const_cast<char*>(ParameterName.data());

    ScilabError = createMatrixOfStringInList(pvApiCtx, Rhs + 1, Address, 3, 1, 1, &NamePointer);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    ScilabError = createMatrixOfDoubleInList(pvApiCtx,
        Rhs + 1,
        Address,
        4,
        1,
        1,
        &NumberOfFrames);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The third item is the frame rate.

    ParameterName = "FrameRate";

    NamePointer = const_cast<char*>(ParameterName.data());

    ScilabError = createMatrixOfStringInList(pvApiCtx, Rhs + 1, Address, 5, 1, 1, &NamePointer);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    ScilabError = createMatrixOfDoubleInList(pvApiCtx, Rhs + 1, Address, 6, 1, 1, &FrameRate);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The fourth item is the frame width.

    ParameterName = "Width";

    NamePointer = const_cast<char*>(ParameterName.data());

    ScilabError = createMatrixOfStringInList(pvApiCtx, Rhs + 1, Address, 7, 1, 1, &NamePointer);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    ScilabError = createMatrixOfDoubleInList(pvApiCtx, Rhs + 1, Address, 8, 1, 1, &Width);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The fifth item is the frame height.

    ParameterName = "Height";

    NamePointer = const_cast<char*>(ParameterName.data());

    ScilabError = createMatrixOfStringInList(pvApiCtx, Rhs + 1, Address, 9, 1, 1, &NamePointer);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    ScilabError = createMatrixOfDoubleInList(pvApiCtx, Rhs + 1, Address, 10, 1, 1, &Height);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The sixth item is IsColor.

    ParameterName = "IsColor";

    NamePointer = const_cast<char*>(ParameterName.data());

    ScilabError = createMatrixOfStringInList(pvApiCtx, Rhs + 1, Address, 11, 1, 1, &NamePointer);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    ScilabError = createMatrixOfBooleanInList(pvApiCtx, Rhs + 1, Address, 12, 1, 1, &IsColor);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // Output argument position is set.
    LhsVar(1) = Rhs + 1;

    return 0;
} // sci_GetVideoInfo
