//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////


#ifndef IPL_IMAGE_TO_SCILAB_H
#define IPL_IMAGE_TO_SCILAB_H


// In OpenCV color images are interleaved whereas in Scilab they consist of three channel 
// matrices. This function converts an interleaved color image to one with three channel
// matrices. In OpenCV the pixels are aligned row-wise whereas in Scilab the pixels are 
// aligned column-wise. Therefore, for each pixel the linear index in the Scilab image is
// calculated from the row, column and channel in the OpenCV image.
template<class GrayValueType>
void IplImageToScilab(const IplImage* Image, 
    GrayValueType** ImageData, 
    TUChar ColorSpace)
{
    // Initialization
    *ImageData = NULL;

    // Memory for image data of destination image is allocated.

    TUInt NumberOfElements = Image->width * Image->height * Image->nChannels;

    *ImageData = new GrayValueType[NumberOfElements];

    if(*ImageData == NULL)
    {
        return;
    } // if

    // Image data are transferred from source to destination.

    TUInt NumberOfPixels = Image->width * Image->height;

    GrayValueType* PixelList = *ImageData;

    if(Image->nChannels > 1)
    {
        // Each pixel consists of three components. Each pixel component is transferred to its
        // channel in the destination image.

        TUInt Step = Image->width * Image->nChannels;

        for(TUInt n = 0; n < NumberOfElements; n++)
        {
            TUInt Row = n / Step;

            TUInt Column = (n % Step) / Image->nChannels;

            TUInt Channel = 0;

            if(ColorSpace == COLOR_SPACE_RGB)
            {
                Channel = Image->nChannels - (n % Image->nChannels) - 1;
            }
            else
            {
                Channel = n % Image->nChannels;
            } // if

            GrayValueType* PixelPointer = PixelList 
                + (n % Image->nChannels) 
                * NumberOfPixels 
                + Column 
                * Image->height 
                + Row;

            const char* BytePointer = Image->imageData 
                + Row * Image->widthStep 
                + (Column * Image->nChannels + Channel) * sizeof(GrayValueType);

            GrayValueType* PixelComponent = (GrayValueType*) BytePointer;

            *PixelPointer = *PixelComponent;
        } // for  
    }
    else
    {
        for(TUInt n = 0; n < NumberOfElements; n++)
        {
            TUInt Row = n / Image->width;

            TUInt Column = n % Image->width;

            GrayValueType* PixelPointer = PixelList + Column * Image->height + Row;

            const char* BytePointer = Image->imageData 
                + Row * Image->widthStep 
                + Column * sizeof(GrayValueType);

            GrayValueType* PixelComponent = (GrayValueType*) BytePointer;

            *PixelPointer = *PixelComponent;
        } // for
    } // if
} // IplImageToScilab

#endif
