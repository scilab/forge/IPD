////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////


#ifndef LINEAR_FILTER_H
#define LINEAR_FILTER_H


#include "MaskFilterTemplate.h"


template<class GrayValueType>
class CLinearFilter : public CMaskFilterTemplate<GrayValueType>
{
public:

    CLinearFilter(TUInt NumberOfColumns, TUInt NumberOfRows, const GrayValueType* const Mask);

protected:
    typedef vector<GrayValueType> TGrayValueList;
    virtual GrayValueType ComputeGrayValue(const TGrayValueList& GrayValues) const;

private:

    const GrayValueType* m_Mask;
}; // CLinearFilter


template<class GrayValueType>
CLinearFilter<GrayValueType>::CLinearFilter(TUInt NumberOfColumns,
    TUInt NumberOfRows,
    const GrayValueType* const Mask):
CMaskFilterTemplate<GrayValueType>(NumberOfColumns, NumberOfRows),
    m_Mask(Mask)
{
} // CLinearFilter::CLinearFilter


template<class GrayValueType>
GrayValueType CLinearFilter<GrayValueType>::ComputeGrayValue(const TGrayValueList& GrayValues) const
{
    const GrayValueType StartValue = 0;

    return inner_product(GrayValues.begin(), GrayValues.end(), m_Mask, StartValue);
} // CLinearFilter::ComputeGrayValue


#endif
