////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#ifndef MASK_FILTER_TEMPLATE_H


#include "IPD.h"
#include "Filter.h"


template<class GrayValueType>
class CMaskFilterTemplate: public CFilter<GrayValueType>
{
public:

    CMaskFilterTemplate(TUInt NumberOfColumns, TUInt NumberOfRows);

    virtual ~CMaskFilterTemplate(){};

protected:
    typedef vector<GrayValueType> TGrayValueList;
    virtual GrayValueType ComputeGrayValue(const TGrayValueList& GrayValues) const = 0;
}; // CMaskFilterTemplate


template<class GrayValueType>
CMaskFilterTemplate<GrayValueType>::CMaskFilterTemplate(TUInt NumberOfColumns,
    TUInt NumberOfRows):
CFilter<GrayValueType>::CFilter(NumberOfColumns, NumberOfRows)
{
    // Central pixel coordinates are calculated.

    TUInt CentralColumn = NumberOfColumns / 2;

    TUInt CentralRow = NumberOfRows / 2;

    // For each non-zero pixel of the filter the horizontal and vertical distances to the cen-
    // tral pixel are calculated and stored in m_ColumnOffsets and m_RowOffsets respectively.

    for(TUInt n = 0; n < NumberOfColumns; n++)
    {
        for(TUInt m = 0; m < NumberOfRows; m++)
        {
            CFilter<GrayValueType>::m_ColumnOffsets.push_back(n - CentralColumn);

            CFilter<GrayValueType>::m_RowOffsets.push_back(m - CentralRow);

            CFilter<GrayValueType>::m_NumberOfElements++;
        } // for
    } // for
} //CMaskFilterTemplate::CMaskFilterTemplate


#define MASK_FILTER_TEMPLATE_H
#endif
