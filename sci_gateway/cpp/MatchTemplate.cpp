////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#include "gw_ipd.h"
#include "IPD.h"
#include "ScilabToIplImage.h"
#include "IplImageToScilab.h"
#include "TransferImageData.h"

extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};



int sci_MatchTemplate(char* fname)
{
    // Input and output parameters are checked.

    CheckRhs(3, 3);

    CheckLhs(1, 1);

    // The first argument is a 2D matrix of type uint8.

    int* Address = NULL;

    SciErr ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &Address);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    TUChar* SourceImageData = NULL;

    int NumberOfSourceRows = 0;

    int NumberOfSourceColumns = 0;

    ScilabError = getMatrixOfUnsignedInteger8(pvApiCtx,
        Address,
        &NumberOfSourceRows,
        &NumberOfSourceColumns,
        &SourceImageData);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The second parameter must be a 2D matrix of type uint8.

    ScilabError = getVarAddressFromPosition(pvApiCtx, 2, &Address);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    TUChar* TemplateImageData = NULL;

    int NumberOfTemplateRows = 0;

    int NumberOfTemplateColumns = 0;

    ScilabError = getMatrixOfUnsignedInteger8(pvApiCtx,
        Address,
        &NumberOfTemplateRows,
        &NumberOfTemplateColumns,
        &TemplateImageData);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The template image must not be bigger than the source image.

    if((NumberOfTemplateRows > NumberOfSourceRows) || (NumberOfTemplateColumns > NumberOfSourceColumns))
    {
        Scierror(ERROR_ARGUMENT, "The template image must not have more rows or columns than the source image.");

        return 0;
    } // if

    // The third argument must be a scalar of type uint8.

    ScilabError = getVarAddressFromPosition(pvApiCtx, 3, &Address);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    TUChar* MatchMethod = NULL;

    int NumberOfMatchMethodRows = 0;

    int NumberOfMatchMethodColumns = 0;

    ScilabError = getMatrixOfUnsignedInteger8(pvApiCtx,
        Address,
        &NumberOfMatchMethodRows,
        &NumberOfMatchMethodColumns,
        &MatchMethod);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if((NumberOfMatchMethodRows != 1) || (NumberOfMatchMethodColumns != 1))
    {
        Scierror(ERROR_ARGUMENT, "The third argument must be a scalar of type uint8.");

        return 0;
    } // if

    // The match method is determined.

    int Method = 0;

    switch(*MatchMethod)
    {
    case MATCH_LEAST_SQUARES:
        {
            Method = CV_TM_SQDIFF;
        } // case

        break;

    case MATCH_CORRELATION:
        {
            Method = CV_TM_CCORR;
        } // case

        break;

    case MATCH_LEAST_SQUARES_NORM:
        {
            Method = CV_TM_SQDIFF_NORMED;
        } // case

        break;

    case MATCH_CORRELATION_NORM:
        {
            Method = CV_TM_CCORR_NORMED;
        } // case

        break;

    default:
        {
            Scierror(ERROR_ARGUMENT, "Unknown template matching method.");

            return 0;
        } // default

        break;
    } // switch

    // The source image is transformed to an OpenCV image.

    IplImage* Source = NULL;

    ScilabToIplImage(&Source,
        SourceImageData,
        NumberOfSourceColumns,
        NumberOfSourceRows,
        1,
        sci_ints,
        SCI_UINT8);

    if(Source == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    // The template image is transformed to an OpenCV image.

    IplImage* Template = NULL;

    ScilabToIplImage(&Template,
        TemplateImageData,
        NumberOfTemplateColumns,
        NumberOfTemplateRows,
        1,
        sci_ints,
        SCI_UINT8);

    if(Template == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        cvReleaseImage(&Source);

        return 0;
    } // if

    // The match result image is generated.

    IplImage* MatchResult = NULL;

    CvSize ResultSize = cvSize(NumberOfSourceColumns - NumberOfTemplateColumns + 1,
        NumberOfSourceRows - NumberOfTemplateRows + 1);

    MatchResult = cvCreateImage(ResultSize, IPL_DEPTH_32F, 1);

    if(MatchResult == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        cvReleaseImage(&Source);

        cvReleaseImage(&Template);

        return 0;
    } // if

    // Template matching is performed now. The result image is transformed to a Scilab image of type double.

    cvMatchTemplate(Source, Template, MatchResult, Method);

    cvReleaseImage(&Source);

    cvReleaseImage(&Template);

    float* ResultImageAsFloat = NULL;

    IplImageToScilab(MatchResult, &ResultImageAsFloat, COLOR_SPACE_GRAY);

    cvReleaseImage(&MatchResult);

    if(ResultImageAsFloat == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    double* ResultImageAsDouble = NULL;

    TransferImageData(ResultSize.height * ResultSize.width,
        ResultImageAsFloat,
        &ResultImageAsDouble);

    delete ResultImageAsFloat;

    if(ResultImageAsDouble == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    // The output argument is created.

    ScilabError = createMatrixOfDouble(pvApiCtx,
        Rhs + 1,
        ResultSize.height,
        ResultSize.width,
        ResultImageAsDouble);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // Output argument position is set.
    LhsVar(1) = Rhs + 1;

    return 0;
}

