////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////


#ifndef MEDIAN_H
#define MEDIAN_H


#include "MaskFilterTemplate.h"


template<class GrayValueType>
class CMedian : public CMaskFilterTemplate<GrayValueType>
{
public:

    CMedian(TUInt NumberOfColumns, TUInt NumberOfRows);

    virtual ~CMedian(){};

protected:
    typedef vector<GrayValueType> TGrayValueList;
    virtual GrayValueType ComputeGrayValue(const TGrayValueList& GrayValues) const;

private:

    const bool m_IsOdd;

    const TUInt m_CentralElement;
}; // CMedian


template<class GrayValueType>
CMedian<GrayValueType>::CMedian(TUInt NumberOfColumns, TUInt NumberOfRows):
CMaskFilterTemplate<GrayValueType>(NumberOfColumns, NumberOfRows),
    m_IsOdd((NumberOfColumns * NumberOfRows) % 2 > 0),
    m_CentralElement((NumberOfColumns * NumberOfRows) / 2)
{
} // CMedian::CMedian


template<class GrayValueType>
GrayValueType CMedian<GrayValueType>::ComputeGrayValue(const TGrayValueList& GrayValues) const
{
    TGrayValueList SortedGrayValues(GrayValues);

    sort(SortedGrayValues.begin(), SortedGrayValues.end());

    if(m_IsOdd)
    {
        return SortedGrayValues[m_CentralElement];
    }
    else
    {
        return (SortedGrayValues[m_CentralElement - 1] + SortedGrayValues[m_CentralElement]) / 2;
    } // if
} // CMedian::ComputeGrayValue


#endif


