////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#include "gw_ipd.h"
#include "IPD.h"
#include "Dilation.h"
#include "Erosion.h"

extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};


template<class GrayValueType>
GrayValueType* MorphologicalFilter(TUChar FilterType,
    const GrayValueType* const OriginalImage,
    int NumberOfColumns,
    int NumberOfRows,
    const int* const StructureElementData,
    int StructureElementWidth,
    int StructureElementHeight);


int sci_MorphologicalFilter(char* fname)
{
    // Input and output parameters are checked.

    CheckRhs(3, 3);

    CheckLhs(1, 1);

    // The first parameter must be a matrix of type bool, uint8, uint16, uint32 or double.

    // The type is determined.

    int* OriginalImageAddress = NULL;

    SciErr ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &OriginalImageAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int ImageType = 0;

    int Precision = 0;

    ScilabError = getVarType(pvApiCtx, OriginalImageAddress, &ImageType);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    switch(ImageType)
    {
    case sci_matrix:
        {
            // Okay.
        } // case

        break;

    case sci_boolean:
        {
            // Okay.
        } // case

        break;

    case sci_ints:
        {
            // Only unsigned integer images are processed by IPD toolbox.

            ScilabError = getMatrixOfIntegerPrecision(pvApiCtx, OriginalImageAddress, &Precision);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                return 0;
            } // if

            switch(Precision)
            {
            case SCI_UINT8:
                {
                    // Okay.
                } // case

                break;

            case SCI_UINT16:
                {
                    // Okay.
                } // case

                break;

            case SCI_UINT32:
                {
                    // Okay.
                } // case

                break;

            default:
                {
                    string Message1("The first argument must be a matrix of type boolean, uint8, uint16, ");

                    string Message2("uint32 or double.");

                    string Message(Message1 + Message2);

                    Scierror(ERROR_ARGUMENT, const_cast<char*>(Message.data()));

                    return 0;
                } // default

                break;
            } // switch
        } // case

        break;

    default:
        {
            string Message1("The first argument must be a matrix of type boolean, uint8, uint16, ");

            string Message2("uint32 or double.");

            string Message(Message1 + Message2);

            Scierror(ERROR_ARGUMENT, const_cast<char*>(Message.data()));

            return 0;
        } // default

        break;
    } // switch

    // The second parameter is the filter type, specified by a uint8 scalar.

    int* FilterTypeAddress = NULL;

    ScilabError = getVarAddressFromPosition(pvApiCtx, 2, &FilterTypeAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    Precision = 0;

    ScilabError = getMatrixOfIntegerPrecision(pvApiCtx, FilterTypeAddress, &Precision);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if(Precision != SCI_UINT8)
    {
        Scierror(ERROR_SCALAR, "The first argument must be a scalar of type uint8.");

        return 0;
    } // if

    int NumberOfRows = 0;

    int NumberOfColumns = 0;

    TUChar* FilterType = NULL;

    ScilabError = getMatrixOfUnsignedInteger8(pvApiCtx,
        FilterTypeAddress,
        &NumberOfRows,
        &NumberOfColumns,
        &FilterType);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if((NumberOfRows != 1) || (NumberOfColumns != 1))
    {
        Scierror(ERROR_SCALAR, "The second argument must be a scalar of type uint8.");

        return 0;
    } // if

    // The third input parameter must be a boolean matrix.

    int* StructureElementAddress = NULL;

    ScilabError = getVarAddressFromPosition(pvApiCtx, 3, &StructureElementAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int StructureElementHeight = 0;

    int StructureElementWidth = 0;

    int* StructureElementData = NULL;

    ScilabError = getMatrixOfBoolean(pvApiCtx,
        StructureElementAddress,
        &StructureElementHeight,
        &StructureElementWidth,
        &StructureElementData);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The morphological filtering is done now.

    switch(ImageType)
    {
    case sci_matrix:
        {
            double* OriginalImageData = NULL;

            ScilabError = getMatrixOfDouble(pvApiCtx,
                OriginalImageAddress,
                &NumberOfRows,
                &NumberOfColumns,
                &OriginalImageData);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                return 0;
            } // if

            double* FilteredImage = MorphologicalFilter(*FilterType,
                OriginalImageData,
                NumberOfColumns,
                NumberOfRows,
                StructureElementData,
                StructureElementWidth,
                StructureElementHeight);

            if(FilteredImage == 0)
            {
                NumberOfRows = 0;

                NumberOfColumns = 0;
            } // if

            ScilabError = createMatrixOfDouble(pvApiCtx,
                Rhs + 1,
                NumberOfRows,
                NumberOfColumns,
                FilteredImage);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                return 0;
            } // if
        } // case

        break;

    case sci_boolean:
        {
            int* OriginalImageData = NULL;

            ScilabError = getMatrixOfBoolean(pvApiCtx,
                OriginalImageAddress,
                &NumberOfRows,
                &NumberOfColumns,
                &OriginalImageData);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                return 0;
            } // if

            int* FilteredImage = MorphologicalFilter(*FilterType,
                OriginalImageData,
                NumberOfColumns,
                NumberOfRows,
                StructureElementData,
                StructureElementWidth,
                StructureElementHeight);

            if(FilteredImage == NULL)
            {
                NumberOfRows = 0;

                NumberOfColumns = 0;
            } // if

            ScilabError = createMatrixOfBoolean(pvApiCtx,
                Rhs + 1,
                NumberOfRows,
                NumberOfColumns,
                FilteredImage);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);
            } // if
        } // case

        break;

    case sci_ints:
        {
            switch(Precision)
            {
            case SCI_UINT8:
                {
                    TUChar* OriginalImageData = NULL;

                    ScilabError = getMatrixOfUnsignedInteger8(pvApiCtx,
                        OriginalImageAddress,
                        &NumberOfRows,
                        &NumberOfColumns,
                        &OriginalImageData);

                    if(ScilabError.iErr)
                    {
                        printError(&ScilabError, 0);

                        return 0;
                    } // if

                    TUChar* FilteredImage = MorphologicalFilter(*FilterType,
                        OriginalImageData,
                        NumberOfColumns,
                        NumberOfRows,
                        StructureElementData,
                        StructureElementWidth,
                        StructureElementHeight);

                    if(FilteredImage == NULL)
                    {
                        NumberOfRows = 0;

                        NumberOfColumns = 0;
                    } // if

                    ScilabError = createMatrixOfUnsignedInteger8(pvApiCtx,
                        Rhs + 1,
                        NumberOfRows,
                        NumberOfColumns,
                        FilteredImage);

                    if(ScilabError.iErr)
                    {
                        printError(&ScilabError, 0);
                    } // if
                } // case

                break;

            case SCI_UINT16:
                {
                    TUShort* OriginalImageData = NULL;

                    ScilabError = getMatrixOfUnsignedInteger16(pvApiCtx,
                        OriginalImageAddress,
                        &NumberOfRows,
                        &NumberOfColumns,
                        &OriginalImageData);

                    if(ScilabError.iErr)
                    {
                        printError(&ScilabError, 0);

                        return 0;
                    } // if

                    TUShort* FilteredImage = MorphologicalFilter(*FilterType,
                        OriginalImageData,
                        NumberOfColumns,
                        NumberOfRows,
                        StructureElementData,
                        StructureElementWidth,
                        StructureElementHeight);

                    if(FilteredImage == NULL)
                    {
                        NumberOfRows = 0;

                        NumberOfColumns = 0;
                    } // if

                    ScilabError = createMatrixOfUnsignedInteger16(pvApiCtx,
                        Rhs + 1,
                        NumberOfRows,
                        NumberOfColumns,
                        FilteredImage);

                    if(ScilabError.iErr)
                    {
                        printError(&ScilabError, 0);
                    } // if
                } // case

                break;

            case SCI_UINT32:
                {
                    TUInt* OriginalImageData = NULL;

                    ScilabError = getMatrixOfUnsignedInteger32(pvApiCtx,
                        OriginalImageAddress,
                        &NumberOfRows,
                        &NumberOfColumns,
                        &OriginalImageData);

                    if(ScilabError.iErr)
                    {
                        printError(&ScilabError, 0);

                        return 0;
                    } // if

                    TUInt* FilteredImage = MorphologicalFilter(*FilterType,
                        OriginalImageData,
                        NumberOfColumns,
                        NumberOfRows,
                        StructureElementData,
                        StructureElementWidth,
                        StructureElementHeight);

                    if(FilteredImage == NULL)
                    {
                        NumberOfRows = 0;

                        NumberOfColumns = 0;
                    } // if

                    ScilabError = createMatrixOfUnsignedInteger32(pvApiCtx,
                        Rhs + 1,
                        NumberOfRows,
                        NumberOfColumns,
                        FilteredImage);

                    if(ScilabError.iErr)
                    {
                        printError(&ScilabError, 0);
                    } // if
                } // case

                break;

            default:
                {
                } // default

                break;
            } // switch
        } // case

        break;

    default:
        {
        } // default

        break;
    } // switch

    // The output argument position is set now.

    LhsVar(1) = Rhs + 1;

    return 0;
} // sci_MorphologicalFilter


template<class GrayValueType>
GrayValueType* MorphologicalFilter(TUChar FilterType,
    const GrayValueType* const OriginalImage,
    int NumberOfColumns,
    int NumberOfRows,
    const int* const StructureElementData,
    int StructureElementWidth,
    int StructureElementHeight)
{
    // Filter is initialized.
    CFilter<GrayValueType>* FirstPass = NULL;

    CFilter<GrayValueType>* SecondPass = NULL;

    switch(FilterType)
    {
    case FILTER_DILATE:
        {
            FirstPass = new CDilation<GrayValueType>(StructureElementWidth,
                StructureElementHeight,
                StructureElementData);

            if(FirstPass == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return NULL;
            } // if
        } // case

        break;

    case FILTER_ERODE:
        {
            FirstPass = new CErosion<GrayValueType>(StructureElementWidth,
                StructureElementHeight,
                StructureElementData);

            if(FirstPass == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return NULL;
            } // if
        } // case

        break;

    case FILTER_CLOSE:
        {
            FirstPass = new CDilation<GrayValueType>(StructureElementWidth,
                StructureElementHeight,
                StructureElementData);

            if(FirstPass == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return NULL;
            } // if

            SecondPass = new CErosion<GrayValueType>(StructureElementWidth,
                StructureElementHeight,
                StructureElementData);

            if(SecondPass == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return NULL;
            } // if
        } // case

        break;

    case FILTER_OPEN:
        {
            FirstPass = new CErosion<GrayValueType>(StructureElementWidth,
                StructureElementHeight,
                StructureElementData);

            if(FirstPass == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return NULL;
            } // if

            SecondPass = new CDilation<GrayValueType>(StructureElementWidth,
                StructureElementHeight,
                StructureElementData);

            if(SecondPass == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return NULL;
            } // if
        } // case

        break;

    case FILTER_TOP_HAT:
        {
            FirstPass = new CErosion<GrayValueType>(StructureElementWidth,
                StructureElementHeight,
                StructureElementData);

            if(FirstPass == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return NULL;
            } // if

            SecondPass = new CDilation<GrayValueType>(StructureElementWidth,
                StructureElementHeight,
                StructureElementData);

            if(SecondPass == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return NULL;
            } // if
        } // case

        break;

    case FILTER_BOTTOM_HAT:
        {
            FirstPass = new CDilation<GrayValueType>(StructureElementWidth,
                StructureElementHeight,
                StructureElementData);

            if(FirstPass == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return NULL;
            } // if

            SecondPass = new CErosion<GrayValueType>(StructureElementWidth,
                StructureElementHeight,
                StructureElementData);

            if(SecondPass == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return NULL;
            } // if
        } // case

        break;

    default:
        {
            string Message1("The third argument must be one of the constansts ");

            string Message2("FILTER_DILATE, FILTER_ERODE, FILTER_CLOSE, FILTER_OPEN, ");

            string Message3("FILTER_TOP_HAT OR FILTER_BOTTOM_HAT.");

            string Message(Message1 + Message2 + Message3);

            Scierror(ERROR_ARGUMENT, const_cast<char*>(Message.data()));

            return NULL;
        } // default

        break;
    } // switch

    // The image is filtered now. If a top hat or bottom hat filter is applied, the difference
    // between the filtered and original image is calculated and stored in the filtered image.

    GrayValueType* FilteredImage = new GrayValueType[NumberOfRows * NumberOfColumns];

    if(FilteredImage == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        if(FirstPass != NULL)
        {
            delete FirstPass;
        } // if

        if(SecondPass != NULL)
        {
            delete SecondPass;
        } // if

        return NULL;
    } // if

    if(FirstPass != NULL)
    {
        FirstPass->FilterImage(NumberOfColumns, NumberOfRows, OriginalImage, FilteredImage);
    } // if

    if(SecondPass != NULL)
    {
        SecondPass->FilterImage(NumberOfColumns, NumberOfRows, FilteredImage, FilteredImage);

        switch(FilterType)
        {
        case FILTER_TOP_HAT:
            {
                TUInt NumberOfPixels = NumberOfColumns * NumberOfRows;

                for(TUInt n = 0; n < NumberOfPixels; n++)
                {
                    FilteredImage[n] = OriginalImage[n] - FilteredImage[n];
                } // for
            } // case

            break;

        case FILTER_BOTTOM_HAT:
            {
                TUInt NumberOfPixels = NumberOfColumns * NumberOfRows;

                for(TUInt n = 0; n < NumberOfPixels; n++)
                {
                    FilteredImage[n] = FilteredImage[n] - OriginalImage[n];
                } // for
            } // case

            break;

        default:
            {
            } // default

            break;
        } // switch
    } // if

    // Memory is released.

    if(FirstPass != NULL)
    {
        delete FirstPass;
    } // if

    if(SecondPass != NULL)
    {
        delete SecondPass;
    } // if

    return FilteredImage;


    return NULL;
} // MorphologicalFilter
