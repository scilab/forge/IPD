////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#include "gw_ipd.h"
#include "IPD.h"
#include "IplImageToScilab.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};

int sci_ReadImageFile(char* fname)
{
    // Input and output parameters are checked.

    CheckRhs(1, 1);

    CheckLhs(2, 2);

    // Address of input parameter (file name) is determined.

    SciErr ScilabError;

    int* FileNamePointer = NULL;

    ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &FileNamePointer);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // Length of file name is determined. An input parameter of type string can be a matrix with
    // several rows and columns. Each element of this matrix is a string. Therefore, the number
    // of columns and rows are checked. A file name should consist of only one single string, so
    // the function returns otherwise.

    int NumberOfRows = 0;

    int NumberOfColumns = 0;

    int StringLength = 0;

    ScilabError = getMatrixOfString(pvApiCtx,
        FileNamePointer,
        &NumberOfRows,
        &NumberOfColumns,
        NULL,
        NULL);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if((NumberOfRows != 1) || (NumberOfColumns != 1))
    {
        Scierror(ERROR_FILE_NAME, "Invalid file name. A file name must be a string.");

        return 0;
    } // if

    // Length of file name is determined.

    ScilabError = getMatrixOfString(pvApiCtx,
        FileNamePointer,
        &NumberOfRows,
        &NumberOfColumns,
        &StringLength,
        NULL);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // File name is retrieved.

    char* FileName = new char[StringLength + 1];

    if(FileName == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    ScilabError = getMatrixOfString(pvApiCtx,
        FileNamePointer,
        &NumberOfRows,
        &NumberOfColumns,
        &StringLength,
        &FileName);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        delete FileName;

        return 0;
    } // if

    // The image file is loaded now.

    IplImage* Image = cvLoadImage(FileName, CV_LOAD_IMAGE_UNCHANGED);

    if(Image == NULL)
    {
        printError(&ScilabError, 0);

        delete FileName;

        return 0;
    } // if

    // The image type is determined and an output variables are created. IPD toolbox handles
    // only unsigned integer and double valued images, but OpenCV does not implement uint32
    // of Scilab. Therefore, images of type uint32 are stored as images of type int32.

    TUInt ImageSize = Image->width * Image->height * Image->nChannels;

    TUChar ColorSpace = COLOR_SPACE_RGB;

    if(Image->nChannels == 1)
    {
        ColorSpace = COLOR_SPACE_GRAY;
    } // if

    switch(Image->depth)
    {
    case IPL_DEPTH_8U:
        {
            TUChar* ImageData = NULL;

            IplImageToScilab(Image, &ImageData, ColorSpace);

            if(ImageData == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                delete FileName;

                cvReleaseImage(&Image);

                return 0;
            } // if

            // The first output parameter is a column vector containing the list of intensity values.

            ScilabError = createMatrixOfUnsignedInteger8(pvApiCtx, Rhs + 1, ImageSize, 1, ImageData);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                delete FileName;

                cvReleaseImage(&Image);

                delete ImageData;

                return 0;
            } // if
        } // case

        break;

    case IPL_DEPTH_16U:
        {
            TUShort* ImageData = NULL;

            IplImageToScilab(Image, &ImageData, ColorSpace);

            if(ImageData == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                delete FileName;

                cvReleaseImage(&Image);

                return 0;
            } // if

            // The first output parameter is a column vector containing the list of intensity values.

            ScilabError = createMatrixOfUnsignedInteger16(pvApiCtx, Rhs + 1, ImageSize, 1, ImageData);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                delete FileName;

                cvReleaseImage(&Image);

                delete ImageData;

                return 0;
            } // if
        } // case

        break;

    case IPL_DEPTH_32S:
        {
            TUInt* ImageData = NULL;

            IplImageToScilab(Image, &ImageData, ColorSpace);

            if(ImageData == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                delete FileName;

                cvReleaseImage(&Image);

                return 0;
            } // if

            // The first output parameter is a column vector containing the list of intensity values.

            ScilabError = createMatrixOfUnsignedInteger32(pvApiCtx, Rhs + 1, ImageSize, 1, ImageData);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                delete FileName;

                cvReleaseImage(&Image);

                delete ImageData;

                return 0;
            } // if
        } // case

        break;

    case IPL_DEPTH_64F:
        {
            double* ImageData = NULL;

            IplImageToScilab(Image, &ImageData, ColorSpace);

            if(ImageData == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                delete FileName;

                cvReleaseImage(&Image);

                return 0;
            } // if

            // The first output parameter is a column vector containing the list of intensity values.

            ScilabError = createMatrixOfDouble(pvApiCtx, Rhs + 1, ImageSize, 1, ImageData);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                delete FileName;

                cvReleaseImage(&Image);

                delete ImageData;

                return 0;
            } // if
        } // case

        break;

    default:
        {
            Scierror(ERROR_FILE_FORMAT, "File format can not be processed by IPD toolbox.");

            delete FileName;

            cvReleaseImage(&Image);

            return 0;
        } // default

        break;
    } // switch

    // Allocated memory is released.

    delete FileName;

    // The second output parameter is a column vector containing the numbers of rows, columns
    // and channels. The Scilab image is the transposed version of the OpenCV image.

    double Dimensions[3];

    Dimensions[0] = Image->height;

    Dimensions[1] = Image->width;

    Dimensions[2] = Image->nChannels;

    cvReleaseImage(&Image);

    ScilabError = createMatrixOfDouble(pvApiCtx, Rhs + 2, 3, 1, Dimensions);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The positions of left hand side variables are set.

    LhsVar(1) = Rhs + 1;

    LhsVar(2) = Rhs + 2;

    return 0;
} // sci_ReadImage
