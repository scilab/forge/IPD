//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////


#ifndef SCILAB_TO_IPLIMAGE_H
#define SCILAB_TO_IPLIMAGE_H

#include "IPD.h"
#include "api_scilab.h"



// In OpenCV color images are interleaved whereas in Scilab they consist of three channel 
// matrices. This function converts an interleaved color image to one with three channel
// matrices. In OpenCV the pixels are aligned row-wise whereas in Scilab the pixels are 
// aligned column-wise. Therefore, for each pixel the linear index in the OpenCV image is
// calculated from the row, column and channel in the Scilab image.
template<class GrayValueType>
void ScilabToIplImage(IplImage** DestinationImage,
    GrayValueType* ImageData,
    TUInt NumberOfColumns, 
    TUInt NumberOfRows,
    TUInt NumberOfChannels,
    int ImageType, 
    int Precision = 0,
    TUChar ColorSpace = COLOR_SPACE_RGB)
{
    // Initialization
    *DestinationImage = NULL;

    // The depth is determined.

    int Depth = 0;

    switch(ImageType)
    {
    case sci_matrix:
        {
            Depth = IPL_DEPTH_64F;
        } // case

        break;

    case sci_boolean:
        {
            Depth = IPL_DEPTH_32S;
        } // case

        break;

    case sci_ints:
        {
            switch(Precision)
            {
            case SCI_UINT8: 
                {
                    Depth = IPL_DEPTH_8U;
                } // case

                break;

            case SCI_UINT16:
                {
                    Depth = IPL_DEPTH_16U;
                } // case

                break;

            case SCI_INT16:
                {
                    Depth = IPL_DEPTH_16S; // needed for int_EdgeFilter
                } // case

                break;

            case SCI_UINT32:
                {
                    Depth = IPL_DEPTH_32S; // OpenCV does not know uint32
                } // case

                break;

            case SCI_INT32:
                {
                    Depth = IPL_DEPTH_32S; // needed for cvWatershed
                } // case

                break;

            default:
                {
                    return;
                } // default

                break;
            } // switch
        } // case

        break;

    case FLOAT_IMAGE:
        {
            Depth = IPL_DEPTH_32F;
        } // case

        break;

    default:
        {
            return;
        } // default

        break;
    } // switch

    IplImage* Image = cvCreateImage(cvSize(NumberOfColumns, NumberOfRows), 
        Depth, 
        NumberOfChannels);

    if(Image == NULL)
    {
        return;
    } // if

    GrayValueType* PixelList = (GrayValueType*) Image->imageData;

    TUInt NumberOfPixels = NumberOfRows * NumberOfColumns;

    TUInt NumberOfElements = NumberOfPixels * NumberOfChannels;

    GrayValueType* PixelComponent = ImageData;

    if(NumberOfChannels > 1)
    {
        // OpenCV color images are interleaved whereas Scilab images consist of three channel
        // matrices. In OpenCV the channel sequence of RGB images is BGR.

        for(TUInt n = 0; n < NumberOfElements; n++)
        {
            TUInt PixelIndex = n % NumberOfPixels;

            TUInt Row = PixelIndex % NumberOfRows;

            TUInt Column = PixelIndex / NumberOfRows;

            TUInt Channel = 0;

            if(ColorSpace == COLOR_SPACE_RGB)
            {
                // In RGB images the channel sequence is BGR.
                Channel = NumberOfChannels - 1 - n / NumberOfPixels;
            }
            else
            {
                // In color spaces other than RGB the channel sequence is the same in OpenCV and Scilab.
                Channel = n / NumberOfPixels;
            } // if

            const char* BytePointer = Image->imageData 
                + Row * Image->widthStep 
                + (Column * Image->nChannels + Channel) * sizeof(GrayValueType);

            GrayValueType* PixelPointer = (GrayValueType*) BytePointer;

            *PixelPointer = *PixelComponent;

            PixelComponent++;
        } // for
    }
    else
    {
        for(TUInt n = 0; n < NumberOfPixels; n++)
        {
            TUInt Row = n % NumberOfRows;

            TUInt Column = n / NumberOfRows;

            const char* BytePointer = Image->imageData 
                + Row * Image->widthStep 
                + Column * sizeof(GrayValueType);

            GrayValueType* PixelPointer = (GrayValueType*) BytePointer;

            *PixelPointer = *PixelComponent;

            PixelComponent++;
        } // for
    } // if

    *DestinationImage = Image;
} // ScilabToIplImage

#endif
