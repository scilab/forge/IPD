////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
#include "LinearFilter.h"
#include "gw_ipd.h"
#include "IPD.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};




int sci_SeparableFilter(char* fname)
{
    // Input and output parameters are checked.

    CheckRhs(3, 3);

    CheckLhs(1, 1);

    // The first argument is a single channel image of type double.

    int* Address = NULL;

    SciErr ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &Address);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int NumberOfRows = 0;

    int NumberOfColumns = 0;

    double* ImageData = NULL;

    ScilabError = getMatrixOfDouble(pvApiCtx,
        Address,
        &NumberOfRows,
        &NumberOfColumns,
        &ImageData);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int NumberOfImageRows = NumberOfRows;

    int NumberOfImageColumns = NumberOfColumns;

    // The second input parameter must be a vector of type double.

    ScilabError = getVarAddressFromPosition(pvApiCtx, 2, &Address);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    double* ColumnVector = NULL;

    ScilabError = getMatrixOfDouble(pvApiCtx,
        Address,
        &NumberOfColumns,
        &NumberOfRows,
        &ColumnVector);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if((NumberOfRows != 1) && (NumberOfColumns != 1))
    {
        Scierror(ERROR_ARGUMENT, "The second argument must be a vector of type double.");

        return 0;
    } // if

    CLinearFilter<double> SecondPass(1, max(NumberOfColumns, NumberOfRows), ColumnVector);

    // The third input parameter must be a vector of type double.

    ScilabError = getVarAddressFromPosition(pvApiCtx, 3, &Address);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    double* RowVector = NULL;

    ScilabError = getMatrixOfDouble(pvApiCtx,
        Address,
        &NumberOfColumns,
        &NumberOfRows,
        &RowVector);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if((NumberOfRows != 1) && (NumberOfColumns != 1))
    {
        Scierror(ERROR_ARGUMENT, "The third argument must be a vector of type double.");

        return 0;
    } // if

    CLinearFilter<double> FirstPass(max(NumberOfColumns, NumberOfRows), 1, RowVector);

    // Image is convolved with row filter.

    TUInt NumberOfPixels = NumberOfImageRows * NumberOfImageColumns;

    double* RowFilteredImage = new double[NumberOfPixels];

    if(RowFilteredImage == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    FirstPass.FilterImage(NumberOfImageColumns,
        NumberOfImageRows,
        ImageData,
        RowFilteredImage);

    // The filtered image is convolved with the column filter.

    double* ColumnFilteredImage = new double[NumberOfPixels];

    if(RowFilteredImage == NULL)
    {
        delete RowFilteredImage;

        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    SecondPass.FilterImage(NumberOfImageColumns,
        NumberOfImageRows,
        RowFilteredImage,
        ColumnFilteredImage);

    delete RowFilteredImage;

    // The output parameter is created.

    ScilabError = createMatrixOfDouble(pvApiCtx,
        Rhs + 1,
        NumberOfImageRows,
        NumberOfImageColumns,
        ColumnFilteredImage);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The output parameter position is set.
    LhsVar(1) = Rhs + 1;

    return 0;
} // sci_SeparableFilter
