//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////


#ifndef TRANSFER_IMAGE_DATA_H
#define TRANSFER_IMAGE_DATA_H


template<class TSource, class TDestination>
void TransferImageData(int NumberOfElements, const TSource* Source, TDestination** Destination)
{
    *Destination = new TDestination[NumberOfElements];

    if(*Destination == NULL)
    {
        return;
    } // if

    TSource* SourceElement = const_cast<TSource*>(Source);

    TDestination* DestinationElement = *Destination;

    for(int n = 0; n < NumberOfElements; n++)
    {
        DestinationElement[n] = static_cast<TDestination>(*SourceElement);

        SourceElement++;
    } // for
} // TransferImageData

#endif