////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
#include "ScilabToIplImage.h"
#include "gw_ipd.h"
#include "IPD.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};

int sci_WriteImageFile(char* fname)
{
    // Input and output parameters are checked.

    CheckRhs(3, 3);

    CheckLhs(1, 1);

    // The first input parameter is a column or row vector containing the color intensities or
    // gray values. The vector must be of data type uint8.

    // The address of the first argument is determined.

    int* ImageAddress = NULL;

    SciErr ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &ImageAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The image data are retrieved. Then it is checked whether or not a vector was passed.

    TUChar* ImageData = NULL;

    int NumberOfRows = 0;

    int NumberOfColumns = 0;

    ScilabError = getMatrixOfUnsignedInteger8(pvApiCtx,
        ImageAddress,
        &NumberOfRows,
        &NumberOfColumns,
        &ImageData);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if((NumberOfRows != 1) && (NumberOfColumns != 1))
    {
        Scierror(ERROR_ARGUMENT, "The first argument must be a vector of type uint8.");

        return 0;
    } // if

    // The second argument is a vector containing exactly three elements. The first is the number
    // of rows, the second is the number of columns, the third is the number of channels. As the
    // Scilab function size returns a vector of type double, the second argument must be of type
    // double, too.

    int* DimensionsAddress = NULL;

    ScilabError = getVarAddressFromPosition(pvApiCtx, 2, &DimensionsAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    double* Dimensions = 0;

    ScilabError = getMatrixOfDouble(pvApiCtx,
        DimensionsAddress,
        &NumberOfRows,
        &NumberOfColumns,
        &Dimensions);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if(!(((NumberOfRows == 3)  && (NumberOfColumns == 1)) ||
        ((NumberOfRows == 1) &&  (NumberOfColumns == 3))))
    {
        string Message1("The second argument must be a vector of type double with exactly three ");

        string Message2("elements.");

        string Message(Message1 + Message2);

        Scierror(ERROR_ARGUMENT, const_cast<char*>(Message.data()));

        return 0;
    } // if

    // The third argument is the file name.

    int* FileNameAddress = NULL;

    ScilabError = getVarAddressFromPosition(pvApiCtx, 3, &FileNameAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int StringLength = 0;

    ScilabError = getMatrixOfString(pvApiCtx,
        FileNameAddress,
        &NumberOfRows,
        &NumberOfColumns,
        NULL,
        NULL);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if((NumberOfRows != 1) || (NumberOfColumns != 1))
    {
        Scierror(ERROR_FILE_NAME, "Invalid file name. A file name must be a string.");

        return 0;
    } // if

    // Length of file name is determined.

    ScilabError = getMatrixOfString(pvApiCtx,
        FileNameAddress,
        &NumberOfRows,
        &NumberOfColumns,
        &StringLength,
        NULL);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // File name is retrieved.

    char* FileName = new char[StringLength + 1];

    if(FileName == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    ScilabError = getMatrixOfString(pvApiCtx,
        FileNameAddress,
        &NumberOfRows,
        &NumberOfColumns,
        &StringLength,
        &FileName);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        delete FileName;

        return 0;
    } // if

    // The image dimensions are determined now.

    int NumberOfImageRows = static_cast<int>(Dimensions[0]);

    int NumberOfImageColumns = static_cast<int>(Dimensions[1]);

    int NumberOfChannels = static_cast<int>(Dimensions[2]);

    // The Scilab image is transformed to an OpenCV image.

    IplImage* Image = NULL;

    ScilabToIplImage(&Image,
        ImageData,
        NumberOfImageColumns,
        NumberOfImageRows,
        NumberOfChannels,
        sci_ints,
        SCI_UINT8);

    if(Image == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        delete FileName;

        return 0;
    } // if

    int Success = cvSaveImage(FileName, Image);

    delete FileName;

    cvReleaseImage(&Image);

    // The output argument is a boolean scalar, implemented as a matrix with one element.

    ScilabError = createMatrixOfBoolean(pvApiCtx, Rhs + 1, 1, 1, &Success);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    LhsVar(1) = Rhs + 1;

    return 0;
} // sci_WriteImageFile
