//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////


function builder_gateway_cpp()

  gw_cpp_table = ['CloseVideoFile', 'sci_CloseVideoFile'; ..
                  'ConvertColorSpace', 'sci_ConvertColorSpace'; ..
                  'CreatePixelIndexList', 'sci_CreatePixelIndexList'; ..
                  'DistanceTransform', 'sci_DistanceTransform'; ..
                  'GetVideoInfo', 'sci_GetVideoInfo'; ..
                  'MaskFilter', 'sci_MaskFilter'; ..
                  'MatchTemplate', 'sci_MatchTemplate'; ..
                  'MedianFilter', 'sci_MedianFilter'; ..
                  'MorphologicalFilter', 'sci_MorphologicalFilter'; ..
                  'OpenVideoFile', 'sci_OpenVideoFile'; ..
                  'ReadImageFile', 'sci_ReadImageFile'; ..
                  'ReadImageFromVideo', 'sci_ReadImageFromVideo'; ..
                  'SearchBlobs', 'sci_SearchBlobs'; ..
                  'SeparableFilter', 'sci_SeparableFilter'; ..
                  'VarianceFilter', 'sci_VarianceFilter'; ..
                  'Watershed', 'sci_Watershed'; ..
                  'WriteImageFile', 'sci_WriteImageFile'; ..
                  ];
                  
  gw_cpp_files = ['WriteImageFile.cpp', ..
                  'Watershed.cpp', ..
                  'VarianceFilter.cpp', ..
                  'SeparableFilter.cpp', ..
                  'SearchBlobs.cpp', ..
                  'ReadImageFromVideo.cpp', ..
                  'ReadImageFile.cpp', ..
                  'OpenVideoFile.cpp', ..
                  'MorphologicalFilter.cpp', ..
                  'MedianFilter.cpp', ..
                  'MatchTemplate.cpp', ..
                  'MaskFilter.cpp', ..
                  'GetVideoInfo.cpp', ..
                  'DistanceTransform.cpp', ..
                  'CreatePixelIndexList.cpp', ..
                  'ConvertColorSpace.cpp', ..
                  'CloseVideoFile.cpp', ..
                  ];

  gw_cpp_path = get_absolute_file_path('builder_gateway_cpp.sce');

  opencv_libs = [];

  if getos() <> 'Windows' then  //linux, Darwin
    headers_h = ["Variance.h", ..
               "TransferImageData.h", ..
               "ScilabToIplImage.h", ..
               "MorphologicalFilterTemplate.h", ..
               "Median.h", ..
               "MaskFilterTemplate.h", ..
               "LinearFilter.h", ..
               "IplImageToScilab.h", ..
               "IPD.h", ..
               "gw_ipd.h", ..
               "Filter.h", ..
               "Erosion.h", ..
               "Dilation.h", ..
               ];
  
    gw_cpp_files = [gw_cpp_files, headers_h];
    if getos() == 'Darwin' then
      if ~isdir("/usr/local/include/opencv") then
        error("Can not find OpenCV. Compiling IPD needs OpenCV");
      end
      inter_cflags = "-DOPENCV_V2 -I/usr/local/include/opencv ";
      inter_ldflags = "-lopencv_core -lopencv_imgproc -lopencv_calib3d -lopencv_video -lopencv_features2d -lopencv_ml -lopencv_highgui -lopencv_objdetect -lopencv_contrib -lopencv_legacy";
    else
      opencv_version = unix_g('pkg-config --modversion opencv');
      if( length(opencv_version) == 0 | ( strtod( strsubst(opencv_version, '.', '')) <= 99.9 ) )
        error(gettext("OpenCV (version >= 1.0.0) is needed for compiling IPD."));
      end;

      if ( strtod( strsubst(opencv_version, '.', '')) < 111 ) then //if opencv version <1.1.1
        inter_cflags = "-DOPENCV_V1 ";
      else
        inter_cflags = "-DOPENCV_V2 ";
      end;
      inter_cflags = inter_cflags + unix_g('pkg-config --cflags opencv');
      inter_ldflags = unix_g('pkg-config --libs opencv');
      if( (length(inter_cflags)==0) | (length(inter_ldflags)==0))
        error("Can not find OpenCV. Compiling IPD needs OpenCV");
      end    
    end
  else
    gw_cpp_files = [gw_cpp_files, "dllIPD.cpp"];
    OPENCV_INCLUDE_ROOT_PATH = fullpath(gw_cpp_path + "../../thirdparty/opencv/windows/include");
    OPENCV_INCLUDE = fullpath(OPENCV_INCLUDE_ROOT_PATH + "/opencv");
    OPENCV2_INCLUDE = fullpath(OPENCV_INCLUDE_ROOT_PATH + "/opencv2");
    inter_cflags = ilib_include_flag([OPENCV_INCLUDE_ROOT_PATH, OPENCV_INCLUDE, OPENCV2_INCLUDE]);
    inter_ldflags = "";
    opencv_libs = [];
  end

  tbx_build_gateway('gw_IPD', ..
                    gw_cpp_table, ..
                    gw_cpp_files, ..
                    gw_cpp_path, ..
                    opencv_libs, ..
                    inter_ldflags, ..
                    inter_cflags);

endfunction
// ====================================================================
builder_gateway_cpp();
clear builder_gateway_cpp;
// ====================================================================
