// Allan CORNET - 2012
//
// unit test CalculateTextureEnergy
//
global IPD_PATH;
global TEXTURE_EDGE_LEVEL;
RGB = ReadImage(IPD_PATH + 'demos\teaset.png');
Image = RGB2Gray(RGB);
TextureImage = CalculateTextureEnergy(Image, TEXTURE_EDGE_LEVEL);
figure();
ShowImage(TextureImage, 'Texture Image', jetcolormap(max(TextureImage)));