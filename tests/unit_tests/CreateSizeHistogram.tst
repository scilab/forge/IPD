// Allan CORNET - 2012
//
// unit test
//
global IPD_PATH;
RGB = ReadImage(IPD_PATH + 'demos\teaset.png');
Image = RGB2Gray(RGB);
ThresholdImage = SegmentByThreshold(Image, 200);
BlobImage = SearchBlobs(ThresholdImage);
[SizeHistogram, ListOfBins] = CreateSizeHistogram(BlobImage);
figure();
plot(ListOfBins, SizeHistogram);
