// Allan CORNET - 2012
//
// unit test
//
global WAVELET_DAUBECHIES_2;
global IPD_PATH;
RGB = ReadImage(IPD_PATH + 'demos\small_teaset.png');
Image = RGB2Gray(RGB);
ListOfWaveletImages = CreateWaveletFrames(Image, WAVELET_DAUBECHIES_2, 3);
