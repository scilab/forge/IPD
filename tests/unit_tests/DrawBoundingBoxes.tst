// Allan CORNET - 2012
//
// unit test
//
global IPD_PATH;
RGB = ReadImage(IPD_PATH + 'demos\teaset.png');
Image = RGB2Gray(RGB);
Image = uint8(255 * ones(size(Image, 1), size(Image, 2))) - Image;
ThresholdedImage = SegmentByThreshold(Image, 100);
BlobImage = SearchBlobs(ThresholdedImage);
FilteredBlobImage = FilterBySize(BlobImage, 10000); // Small objects are removed.
IsCalculated = CreateFeatureStruct(%f); // Feature struct is generated.
IsCalculated.BoundingBox = %t; // The bounding box shall be calculated for each blob.
BlobStatistics = AnalyzeBlobs(FilteredBlobImage, IsCalculated);
FigureWindow = figure();
ShowImage(FilteredBlobImage, 'Blob Image', jetcolormap(4));
DrawBoundingBoxes(BlobStatistics, [1 1 1], FigureWindow); // Bounding boxes are drawn in white.
 
 
