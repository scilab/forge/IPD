// Allan CORNET - 2012
//
// unit test
//
Image = 0.5 * ones(9, 9); // generate gray image
Image(:, 3) = 0; // draw dark line
Image(:, 7) = 1; // draw light line
StructureElement = CreateStructureElement('square', 3); // generate structuring element
ResultImage = ErodeImage(Image, StructureElement)