// Allan CORNET - 2012
//
// unit test
//

IndexImage = matrix(1 : 256, [16 16]);
ColorMap = jetcolormap(256);
RGB = Ind2RGB(IndexImage, ColorMap);
WriteImage(RGB, TMPDIR +'/test.png');
assert_checktrue(isfile(TMPDIR +'/test.png'));