// Allan CORNET - 2012
//
// unit test
//
global MATCH_CORRELATION_NORM;
global IPD_PATH;
SourceColorImage = ReadImage(IPD_PATH + 'demos\teaset.png');
SourceImage = RGB2Gray(SourceColorImage);
TemplateColorImage = ReadImage(IPD_PATH + 'demos\cropped_image.png');
TemplateImage = RGB2Gray(TemplateColorImage);
Match = MatchTemplate(SourceImage, TemplateImage, MATCH_CORRELATION_NORM);
MatchList = FindBestMatches(Match, size(TemplateImage), MATCH_CORRELATION_NORM);
SquareList = list();
for n = 1 : size(MatchList, 1)
 SquareList($ + 1) = struct('BoundingBox', cat(2, MatchList(n, :) - [1 1], [3 3])');
end;
SourceHandle = figure();
ShowColorImage(SourceColorImage, 'Image with Marked Match Positions');
DrawBoundingBoxes(SquareList, [0 1 0], SourceHandle);
TemplateHandle = figure();
ShowColorImage(TemplateColorImage, 'Template Image');
