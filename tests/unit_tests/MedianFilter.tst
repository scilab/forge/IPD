// Allan CORNET - 2012
//
// unit test
//
OriginalImage = zeros(9, 9);      // Image is generated
OriginalImage(4 : 6, 4 : 6) = 1;  // block of object pixels is generated
OriginalImage(5, 2) = 1;          // single object pixel is set, will disappear after filtering
FilterSize = [3 3];               // filter has three rows and three columns
FilteredImage = MedianFilter(OriginalImage, FilterSize)
