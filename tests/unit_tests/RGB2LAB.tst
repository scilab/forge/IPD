// Allan CORNET - 2012
//
// unit test
//
stacksize(1e7);
global IPD_PATH;
RGB = ReadImage(IPD_PATH + 'demos\teaset.png');
RGBD = double(RGB) / 255;
LAB = RGB2LAB(RGBD);
figure();
ShowColorImage(LAB, 'L*a*b*');
