// Allan CORNET - 2012
//
// unit test
//
Image = rand(3, 3); // generate random image
Threshold = CalculateOtsuThreshold(Image); // determine a threshold
SegmentedImage = SegmentByThreshold(Image, Threshold)