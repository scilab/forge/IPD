// Allan CORNET - 2012
//
// unit test
//
global IPD_PATH;
OriginalImage = ReadImage(IPD_PATH + 'demos\teaset.png'); // image is loaded
WriteImage(OriginalImage, TMPDIR + '\copy.png'); // copy of image is saved
CopiedImage = ReadImage(TMPDIR + '\copy.png'); // copy is read
figure();
ShowColorImage(OriginalImage, 'Original'); // original image is displayed
figure();
ShowColorImage(CopiedImage, 'Copy'); // copy is displayed
