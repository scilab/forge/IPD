// Allan CORNET - 2012
//
// unit test
//
TestImageData = uint8(0 : 255); // gray values of test image are generated
Dimensions = [16 16 1];
global IPD_PATH;
ImagePath = TMPDIR + 'test.png';
Success = WriteImageFile(TestImageData, Dimensions, ImagePath);